<?php

namespace Lerp\OperatingLicense;

use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\I18n\Translator\Loader\PhpArray;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseCreateAjaxController;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseFileAjaxController;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseSearchAjaxController;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseListsAjaxController;
use Lerp\OperatingLicense\Controller\Rest\Files\FileOperatingLicenseRestController;
use Lerp\OperatingLicense\Controller\Rest\OperatingLicenseOrderItemRestController;
use Lerp\OperatingLicense\Controller\Stream\OperatingLicenseStreamController;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;
use Lerp\OperatingLicense\Factory\Controller\Ajax\OperatingLicenseCreateAjaxControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Ajax\OperatingLicenseFileAjaxControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Ajax\OperatingLicenseSearchAjaxControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Ajax\OperatingLicenseListsAjaxControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Rest\Files\FileOperatingLicenseRestControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Rest\OperatingLicenseOrderItemRestControllerFactory;
use Lerp\OperatingLicense\Factory\Controller\Stream\OperatingLicenseStreamControllerFactory;
use Lerp\OperatingLicense\Factory\Entity\ParamsOperatingLicenseItemEntityFactory;
use Lerp\OperatingLicense\Factory\Entity\ParamsOperatingLicenseItemSearchEntityFactory;
use Lerp\OperatingLicense\Factory\Form\Files\FileOperatingLicenseFormFactory;
use Lerp\OperatingLicense\Factory\Form\ViewOperatingLicenseItemFormFactory;
use Lerp\OperatingLicense\Factory\Service\Creator\OperatingLicenseCreatorFactory;
use Lerp\OperatingLicense\Factory\Service\Files\FileOperatingLicenseRelServiceFactory;
use Lerp\OperatingLicense\Factory\Service\OperatingLicenseAccumulateServiceFactory;
use Lerp\OperatingLicense\Factory\Service\OperatingLicenseItemServiceFactory;
use Lerp\OperatingLicense\Factory\Service\OperatingLicenseServiceFactory;
use Lerp\OperatingLicense\Factory\Table\Files\FileOperatingLicenseRelTableFactory;
use Lerp\OperatingLicense\Factory\Table\Files\ViewOperatingLicenseFileOrderTableFactory;
use Lerp\OperatingLicense\Factory\Table\Files\ViewOperatingLicenseFileStockTableFactory;
use Lerp\OperatingLicense\Factory\Table\Files\ViewOperatingLicenseFileTableFactory;
use Lerp\OperatingLicense\Factory\Table\OperatingLicenseItemTableFactory;
use Lerp\OperatingLicense\Factory\Table\OperatingLicenseTableFactory;
use Lerp\OperatingLicense\Factory\Table\OperatingLicenseTypeTableFactory;
use Lerp\OperatingLicense\Factory\Table\ViewOperatingLicenseItemOrderTableFactory;
use Lerp\OperatingLicense\Factory\Table\ViewOperatingLicenseItemStockTableFactory;
use Lerp\OperatingLicense\Factory\Table\ViewOperatingLicenseItemTableFactory;
use Lerp\OperatingLicense\Factory\Unique\UniqueNumberProviderFactory;
use Lerp\OperatingLicense\Form\Files\FileOperatingLicenseForm;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\Creator\OperatingLicenseCreatorInterface;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;
use Lerp\OperatingLicense\Table\Files\FileOperatingLicenseRelTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileOrderTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileStockTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileTable;
use Lerp\OperatingLicense\Table\OperatingLicenseItemTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemOrderTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemStockTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProviderInterface;

return [
    'router'                 => [
        'routes' => [
            /*
             * REST
             */
            'lerp_operatinglicense_rest_orderitem'                                => [
                'type'         => Segment::class,
                'options'      => [
                    'route'       => '/lerp-operating-license-rest-orderitem[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseOrderItemRestController::class,
                    ],
                ],
                'rightsnroles' => [
                    Rightsnroles::KEY_REST => [
                        Rightsnroles::METHOD_GETP   => [
                            Rightsnroles::KEY_ROLEMIN => 3,
                        ],
                        Rightsnroles::METHOD_POST   => [
                            Rightsnroles::KEY_ROLEMIN => 3,
                        ],
                        Rightsnroles::METHOD_UPDATE => [
                            Rightsnroles::KEY_ROLEMIN => 4,
                            Rightsnroles::KEY_GROUPS  => [
                                'project_manager'
                            ],
                        ],
                        Rightsnroles::METHOD_DELETE => [
                            Rightsnroles::KEY_ROLE => 1,
                        ],
                    ],
                ],
            ],
            'lerp_operatinglicense_rest_files_rel'                                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-operating-license[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileOperatingLicenseRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX - lists
             */
            'lerp_operatinglicense_ajax_lists_statusworks'                        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-operating-license-lists-status-works',
                    'defaults' => [
                        'controller' => OperatingLicenseListsAjaxController::class,
                        'action'     => 'statusWorks'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_operatinglicense_type'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-lists-types-assoc[/:ol_origin]',
                    'constraints' => [
                        'ol_origin' => '(production|maintenance)+', // enum_operating_license_origin
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseListsAjaxController::class,
                        'action'     => 'typesAssoc'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_lists_origins'                            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-operating-license-lists-origins',
                    'defaults' => [
                        'controller' => OperatingLicenseListsAjaxController::class,
                        'action'     => 'origins'
                    ],
                ],
            ],
            /*
             * AJAX
             */
            'lerp_operatinglicense_ajax_create_create'                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-create[/:operating_license_item_uuid]',
                    'constraints' => [
                        'operating_license_item_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseCreateAjaxController::class,
                        'action'     => 'create'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_create_replaceforstock'                   => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-operating-license-replace-for-stock',
                    'defaults' => [
                        'controller' => OperatingLicenseCreateAjaxController::class,
                        'action'     => 'replaceForStock'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_files_getfiles'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-files-get[/:operating_license_uuid]',
                    'constraints' => [
                        'operating_license_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseFileAjaxController::class,
                        'action'     => 'getFiles'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_files_getorderfiles'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-order-files-get[/:order_uuid]',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseFileAjaxController::class,
                        'action'     => 'getOrderFiles'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_files_filesorder'                         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-files-order[/:order_uuid]',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseFileAjaxController::class,
                        'action'     => 'getViewOperatingLicenseFilesOrder'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_files_filesstockin'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-files-stockin[/:stockin_uuid]',
                    'constraints' => [
                        'stockin_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseFileAjaxController::class,
                        'action'     => 'getViewOperatingLicenseFilesStock'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_files_filesstockinfactoryorder'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-files-stockin-factoryorder[/:fo_uuid]',
                    'constraints' => [
                        'fo_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseFileAjaxController::class,
                        'action'     => 'getViewOperatingLicenseFilesStockFactoryorder'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_search_operatinglicenseitem_search_order' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-operating-license-item-search-order',
                    'defaults' => [
                        'controller' => OperatingLicenseSearchAjaxController::class,
                        'action'     => 'searchOperatingLicenseItemOrder'
                    ],
                ],
            ],
            'lerp_operatinglicense_ajax_search_operatinglicenseitem_search_stock' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-operating-license-item-search-stock',
                    'defaults' => [
                        'controller' => OperatingLicenseSearchAjaxController::class,
                        'action'     => 'searchOperatingLicenseItemStock'
                    ],
                ],
            ],
            /*
             * Stream
             */
            'lerp_operatinglicense_stream_filestream_stream'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-operating-license-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => OperatingLicenseStreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
        ],
    ],
    'controllers'            => [
        'factories'  => [
            // REST
            OperatingLicenseOrderItemRestController::class => OperatingLicenseOrderItemRestControllerFactory::class,
            FileOperatingLicenseRestController::class      => FileOperatingLicenseRestControllerFactory::class,
            // AJAX
            OperatingLicenseListsAjaxController::class     => OperatingLicenseListsAjaxControllerFactory::class,
            OperatingLicenseCreateAjaxController::class    => OperatingLicenseCreateAjaxControllerFactory::class,
            OperatingLicenseFileAjaxController::class      => OperatingLicenseFileAjaxControllerFactory::class,
            OperatingLicenseSearchAjaxController::class    => OperatingLicenseSearchAjaxControllerFactory::class,
            // stream
            OperatingLicenseStreamController::class        => OperatingLicenseStreamControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager'        => [
        'factories'  => [
            // table
            OperatingLicenseTable::class                  => OperatingLicenseTableFactory::class,
            OperatingLicenseTypeTable::class              => OperatingLicenseTypeTableFactory::class,
            OperatingLicenseItemTable::class              => OperatingLicenseItemTableFactory::class,
            ViewOperatingLicenseItemTable::class          => ViewOperatingLicenseItemTableFactory::class,
            ViewOperatingLicenseItemOrderTable::class     => ViewOperatingLicenseItemOrderTableFactory::class,
            ViewOperatingLicenseItemStockTable::class     => ViewOperatingLicenseItemStockTableFactory::class,
            FileOperatingLicenseRelTable::class           => FileOperatingLicenseRelTableFactory::class,
            ViewOperatingLicenseFileTable::class          => ViewOperatingLicenseFileTableFactory::class,
            ViewOperatingLicenseFileOrderTable::class     => ViewOperatingLicenseFileOrderTableFactory::class,
            ViewOperatingLicenseFileStockTable::class     => ViewOperatingLicenseFileStockTableFactory::class,
            // service
            OperatingLicenseService::class                => OperatingLicenseServiceFactory::class,
            OperatingLicenseItemService::class            => OperatingLicenseItemServiceFactory::class,
            OperatingLicenseAccumulateService::class      => OperatingLicenseAccumulateServiceFactory::class,
            FileOperatingLicenseRelService::class         => FileOperatingLicenseRelServiceFactory::class,
            // service interface
            OperatingLicenseCreatorInterface::class       => OperatingLicenseCreatorFactory::class,
            // entity
            ParamsOperatingLicenseItemEntity::class       => ParamsOperatingLicenseItemEntityFactory::class,
            ParamsOperatingLicenseItemSearchEntity::class => ParamsOperatingLicenseItemSearchEntityFactory::class,
            // form
            ViewOperatingLicenseItemForm::class           => ViewOperatingLicenseItemFormFactory::class,
            FileOperatingLicenseForm::class               => FileOperatingLicenseFormFactory::class,
            // unique
            UniqueNumberProviderInterface::class          => UniqueNumberProviderFactory::class,
        ],
        'invokables' => [],
    ],
    'translator'             => [
        'translation_file_patterns' => [
            /*
             * Das hier kann auch in der Module.php->onBootstrap()
             * $this->translator->addTranslationFilePattern($type, $baseDir, $pattern)
             *
             * zum kategorisieren kann man z.B.:
             * 'pattern'  => 'bitkorn_%s.mo'
             * ...%s ist der language code
             */
            [
                'type'        => PhpArray::class,
                'base_dir'    => __DIR__ . '/../language',
                'pattern'     => '%s.php',
                'text_domain' => 'lerp_operatinglicense'
            ],
        ],
    ],
    'view_helpers'           => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'           => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_operating_license' => [
        'module_brand'                 => 'operatingLicense',
        'status_work'                  => [
            'production'  => [
                'new'       => 'NEW',
                'prototype' => 'PROTOTYPE',
            ],
            'maintenance' => [
                'repaired'   => 'Repaired',
                'modified'   => 'Modified',
                'overhauled' => 'Overhauled',
                'inspected'  => 'Inspected/Tested',
            ],
        ],
        'file_category_id_compute'     => 7,
        'file_category_id_signed'      => 8,
    ],
];

<?php

namespace Lerp\OperatingLicense\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Service\Creator\OperatingLicenseCreatorInterface;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Stock\Service\StockService;

class OperatingLicenseService extends AbstractService
{
    use OperatingLicenseServiceTrait;

    /**
     * @todo use $operatingLicenseSourceType instead
     */
    const SOURCE_TYPE_ORDER_ITEM = 1;
    /**
     * @todo use $operatingLicenseSourceType instead
     */
    const SOURCE_TYPE_STOCKIN = 2;

    /**
     * @todo use $operatingLicenseSourceType instead
     */
    public static array $sourceTypes = [
        self::SOURCE_TYPE_ORDER_ITEM => 'order',
        self::SOURCE_TYPE_STOCKIN    => 'stock',
    ];

    protected OperatingLicenseCreatorInterface $operatingLicenseCreator;
    protected StockService $stockService;
    protected OrderItemService $orderItemService;

    public function setOperatingLicenseCreator(OperatingLicenseCreatorInterface $operatingLicenseCreator): void
    {
        $this->operatingLicenseCreator = $operatingLicenseCreator;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * @param string $origin One of the values in database ENUM enum_operating_license_origin
     * @return array
     */
    public function getOperatingLicenseTypeAssoc(string $origin = ''): array
    {
        return $this->operatingLicenseTypeTable->getOperatingLicenseTypeAssoc($origin);
    }

    /**
     * @param string $operatingLicenseOrigin
     * @return bool
     */
    public function validOperatingLicenseOrigin(string $operatingLicenseOrigin): bool
    {
        return in_array($operatingLicenseOrigin, $this->operatingLicenseOrigins);
    }

    /**
     * @param string $operatingLicenseUuid
     * @return bool
     */
    public function isOperatingLicenseNumber(string $operatingLicenseUuid): bool
    {
        return !empty($ol = $this->operatingLicenseTable->getOperatingLicense($operatingLicenseUuid)) && !empty($ol['operating_license_number']);
    }

    /**
     * 1. start transaction
     * 2. generate & save operating license number
     * 3. call `$this->operatingLicenseCreator->createOperatingLicense()`
     * 4. commit | rollback transaction
     *
     * @param string $operatingLicenseItemUuid
     * @param bool $withTransaction
     * @return bool
     */
    public function createOperatingLicense(string $operatingLicenseItemUuid, bool $withTransaction = true): bool
    {
        $operatingLicenseItemEntity = new ViewOperatingLicenseItemEntity();
        if (empty($viewOperatingLicenseItem = $this->viewOperatingLicenseItemTable->getViewOperatingLicenseItem($operatingLicenseItemUuid))
            || !$operatingLicenseItemEntity->exchangeArrayFromDatabase($viewOperatingLicenseItem)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() - Unable to get ViewOperatingLicenseItem.');
            return false;
        }
        if ($withTransaction) {
            $conn = $this->beginTransaction($this->operatingLicenseTable);
        }
        if (empty($this->uniqueNumberProvider->computeGetNumberComplete($operatingLicenseItemEntity->getOperatingLicenseTypeId()))) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() - Unable to create a unique number.');
            $this->message = 'Unable to create a unique number.';
            return false;
        }
        $operatingLicenseItemEntity->setOperatingLicenseNumber($this->uniqueNumberProvider->getNumber());
        $operatingLicenseItemEntity->setOperatingLicenseNumberPrefix($this->uniqueNumberProvider->getPrefix());
        $operatingLicenseItemEntity->setOperatingLicenseNumberComplete($this->uniqueNumberProvider->getNumberComplete());
        if (!$this->operatingLicenseTable->updateOperatingLicenseNumber($operatingLicenseItemEntity)) {
            if ($withTransaction) {
                $conn->rollback();
            }
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() - Unable to update the unique number.');
            $this->message = 'Unable to update the unique number.';
            return false;
        }
        $this->operatingLicenseCreator->setOperatingLicenseItemEntities([$operatingLicenseItemEntity]);
        if (!$this->operatingLicenseCreator->createOperatingLicense()) {
            if ($withTransaction) {
                $conn->rollback();
            }
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() - Unable to create a operating license.');
            $this->message = 'Unable to create a operating license.';
            return false;
        }
        if ($withTransaction) {
            $conn->commit();
        }
        return true;
    }

}

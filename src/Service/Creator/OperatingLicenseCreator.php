<?php

namespace Lerp\OperatingLicense\Service\Creator;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Exception;
use Laminas\Db\Adapter\Adapter;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Stock\Service\StockService;

/**
 * A Dummy class ...please override it.
 */
class OperatingLicenseCreator extends AbstractService implements OperatingLicenseCreatorInterface
{
    protected Adapter $dbAdapter;
    /**
     * @var string Path to the application file folder
     */
    protected string $filePath;
    protected string $folderBrand;
    protected array $operatingLicenseTypeAssoc;
    protected FileService $fileService;
    protected StockService $stockService;
    protected OrderService $orderService;
    protected UserEquipService $userEquipService;
    protected FileOperatingLicenseRelService $fileOperatingLicenseRelService;

    /**
     * @var ViewOperatingLicenseItemEntity[]
     */
    protected array $operatingLicenseItemEntities;

    public function setDbAdapter(Adapter $dbAdapter): void
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @param string $filePath
     * @return void
     */
    public function setFilePath(string $filePath): void
    {
        $this->filePath = realpath($filePath);
    }

    public function setFolderBrand(string $folderBrand): void
    {
        $this->folderBrand = $folderBrand;
    }

    public function setOperatingLicenseTypeAssoc(array $operatingLicenseTypeAssoc): void
    {
        $this->operatingLicenseTypeAssoc = $operatingLicenseTypeAssoc;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setUserEquipService(UserEquipService $userEquipService): void
    {
        $this->userEquipService = $userEquipService;
    }

    public function setFileOperatingLicenseRelService(FileOperatingLicenseRelService $fileOperatingLicenseRelService): void
    {
        $this->fileOperatingLicenseRelService = $fileOperatingLicenseRelService;
    }

    /**
     * @param ViewOperatingLicenseItemEntity[] $viewOperatingLicenseItemEntities
     * @return void
     */
    public function setOperatingLicenseItemEntities(array $viewOperatingLicenseItemEntities): void
    {
        $this->operatingLicenseItemEntities = $viewOperatingLicenseItemEntities;
    }

    /**
     * It creates nothing ...please override it.
     * It checks:
     * - $this->filePath
     * - $this->operatingLicenseItemEntities
     *
     * @throws Exception
     */
    public function createOperatingLicense(): bool
    {
        if (!isset($this->filePath) || !file_exists($this->filePath)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Path does not exist: ' . $this->filePath);
            return false;
        }
        if (empty($this->operatingLicenseItemEntities) || empty($this->operatingLicenseItemEntities[0]) || empty($this->operatingLicenseItemEntities[0]->getStorage())) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() OperatingLicenseItemEntities are not valid.');
            return false;
        }
        return true;
    }

    /**
     * @throws Exception
     */
    public function createOperatingLicenseForOrder(): bool
    {
        return $this->createOperatingLicense();
    }

    /**
     * @throws Exception
     */
    public function createOperatingLicenseForOrderItem(): bool
    {
        return $this->createOperatingLicense();
    }

    /**
     * @throws Exception
     */
    public function createOperatingLicenseForStockin(): bool
    {
        return $this->createOperatingLicense();
    }
}

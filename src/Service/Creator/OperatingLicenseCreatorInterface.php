<?php

namespace Lerp\OperatingLicense\Service\Creator;

use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;

interface OperatingLicenseCreatorInterface
{
    const FILENAME_EASA_21G = 'FormOne_EASA_21G';
    const FILENAME_EASA_145 = 'FormOne_EASA_145';
    const FILENAME_LBA_21G = 'FormOne_LBA_21G';
    const FILENAME_LBA_145 = 'FormOne_LBA_145';
    const FILENAME_COC = 'CoC';

    /**
     * @param ViewOperatingLicenseItemEntity[] $viewOperatingLicenseItemEntities
     * @return void
     */
    public function setOperatingLicenseItemEntities(array $viewOperatingLicenseItemEntities): void;

    public function createOperatingLicense(): bool;

    public function createOperatingLicenseForOrder(): bool;

    public function createOperatingLicenseForOrderItem(): bool;

    public function createOperatingLicenseForStockin(): bool;

}

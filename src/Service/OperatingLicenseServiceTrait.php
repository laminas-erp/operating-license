<?php

namespace Lerp\OperatingLicense\Service;

use Lerp\OperatingLicense\Table\OperatingLicenseItemTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemOrderTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemStockTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProviderInterface;

trait OperatingLicenseServiceTrait
{
    /**
     * @var array From database enum enum_operating_license_origin
     */
    protected array $operatingLicenseOrigins;
    protected array $statusWorkAssoc = [];
    protected array $statusWorkProductionAssoc = [];
    protected array $statusWorkMaintenanceAssoc = [];

    protected OperatingLicenseTable $operatingLicenseTable;
    protected OperatingLicenseItemTable $operatingLicenseItemTable;
    protected OperatingLicenseTypeTable $operatingLicenseTypeTable;
    protected array $operatingLicenseTypeIdAssoc;
    protected ViewOperatingLicenseItemTable $viewOperatingLicenseItemTable;
    protected ViewOperatingLicenseItemOrderTable $viewOperatingLicenseItemOrderTable;
    protected ViewOperatingLicenseItemStockTable $viewOperatingLicenseItemStockTable;
    protected UniqueNumberProviderInterface $uniqueNumberProvider;

    /**
     * @param array $operatingLicenseOrigins From database enum enum_operating_license_origin
     * @return void
     */
    public function setOperatingLicenseOrigins(array $operatingLicenseOrigins): void
    {
        $this->operatingLicenseOrigins = [];
        foreach($operatingLicenseOrigins as $operatingLicenseOrigin) {
            $this->operatingLicenseOrigins[$operatingLicenseOrigin] = $operatingLicenseOrigin;
        }
    }

    /**
     * @return array From database enum enum_operating_license_origin
     */
    public function getOperatingLicenseOrigins(): array
    {
        return $this->operatingLicenseOrigins;
    }

    public function setStatusWorkAssoc(array $statusWorkAssoc): void
    {
        $this->statusWorkAssoc = $statusWorkAssoc;
        $this->statusWorkProductionAssoc = $this->statusWorkAssoc['production'];
        $this->statusWorkMaintenanceAssoc = $this->statusWorkAssoc['maintenance'];
    }

    public function getStatusWorkAssoc(): array
    {
        return $this->statusWorkAssoc;
    }

    public function getStatusWorkProductionAssoc(): array
    {
        return $this->statusWorkProductionAssoc;
    }

    public function getStatusWorkMaintenanceAssoc(): array
    {
        return $this->statusWorkMaintenanceAssoc;
    }

    public function setOperatingLicenseTable(OperatingLicenseTable $operatingLicenseTable): void
    {
        $this->operatingLicenseTable = $operatingLicenseTable;
    }

    public function setOperatingLicenseItemTable(OperatingLicenseItemTable $operatingLicenseItemTable): void
    {
        $this->operatingLicenseItemTable = $operatingLicenseItemTable;
    }

    public function setOperatingLicenseTypeTable(OperatingLicenseTypeTable $operatingLicenseTypeTable): void
    {
        $this->operatingLicenseTypeTable = $operatingLicenseTypeTable;
    }

    public function setOperatingLicenseTypeIdAssoc(array $operatingLicenseTypeIdAssoc): void
    {
        $this->operatingLicenseTypeIdAssoc = $operatingLicenseTypeIdAssoc;
    }

    public function setViewOperatingLicenseItemTable(ViewOperatingLicenseItemTable $viewOperatingLicenseItemTable): void
    {
        $this->viewOperatingLicenseItemTable = $viewOperatingLicenseItemTable;
    }

    public function setViewOperatingLicenseItemOrderTable(ViewOperatingLicenseItemOrderTable $viewOperatingLicenseItemOrderTable): void
    {
        $this->viewOperatingLicenseItemOrderTable = $viewOperatingLicenseItemOrderTable;
    }

    public function setViewOperatingLicenseItemStockTable(ViewOperatingLicenseItemStockTable $viewOperatingLicenseItemStockTable): void
    {
        $this->viewOperatingLicenseItemStockTable = $viewOperatingLicenseItemStockTable;
    }

    public function setUniqueNumberProvider(UniqueNumberProviderInterface $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }
}

<?php

namespace Lerp\OperatingLicense\Service\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\OperatingLicense\Table\Files\FileOperatingLicenseRelTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileOrderTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileStockTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProvider;

class FileOperatingLicenseRelService extends AbstractService
{
    protected string $moduleBrand = '';
    protected FileOperatingLicenseRelTable $fileOperatingLicenseRelTable;
    protected ViewOperatingLicenseFileTable $viewOperatingLicenseFileTable;
    protected ViewOperatingLicenseFileOrderTable $viewOperatingLicenseFileOrderTable;
    protected ViewOperatingLicenseFileStockTable $viewOperatingLicenseFileStockTable;
    protected FileService $fileService;
    protected UniqueNumberProvider $uniqueNumberProvider;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFileOperatingLicenseRelTable(FileOperatingLicenseRelTable $fileOperatingLicenseRelTable): void
    {
        $this->fileOperatingLicenseRelTable = $fileOperatingLicenseRelTable;
    }

    public function setViewOperatingLicenseFileTable(ViewOperatingLicenseFileTable $viewOperatingLicenseFileTable): void
    {
        $this->viewOperatingLicenseFileTable = $viewOperatingLicenseFileTable;
    }

    public function setViewOperatingLicenseFileOrderTable(ViewOperatingLicenseFileOrderTable $viewOperatingLicenseFileOrderTable): void
    {
        $this->viewOperatingLicenseFileOrderTable = $viewOperatingLicenseFileOrderTable;
    }

    public function setViewOperatingLicenseFileStockTable(ViewOperatingLicenseFileStockTable $viewOperatingLicenseFileStockTable): void
    {
        $this->viewOperatingLicenseFileStockTable = $viewOperatingLicenseFileStockTable;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setUniqueNumberProvider(UniqueNumberProvider $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @param string $fileOperatingLicenseRelUuid
     * @return array
     */
    public function getFileOperatingLicenseRel(string $fileOperatingLicenseRelUuid): array
    {
        return $this->fileOperatingLicenseRelTable->getFileOperatingLicenseRel($fileOperatingLicenseRelUuid);
    }

    public function getFileOperatingLicenseRelJoined(string $fileOperatingLicenseRelUuid): array
    {
        return $this->fileOperatingLicenseRelTable->getFileOperatingLicenseRelJoined($fileOperatingLicenseRelUuid);
    }

    /**
     * For always created files.
     *
     * @param FileEntity $fileEntity
     * @param string $operatingLicenseUuid
     * @return string
     */
    public function handleFileInsert(FileEntity $fileEntity, string $operatingLicenseUuid): string
    {
        if (empty($fileUuid = $this->fileService->insertFile($fileEntity))) {
            return '';
        }
        return $this->insertFileOperatingLicenseRel($fileUuid, $operatingLicenseUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $operatingLicenseUuid
     * @param bool $operatingLicenseSigned
     * @return string fileOperatingLicenseRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $operatingLicenseUuid, bool $operatingLicenseSigned = false): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $this->moduleBrand))) {
            return '';
        }
        return $this->insertFileOperatingLicenseRel($fileUuid, $operatingLicenseUuid, $operatingLicenseSigned);
    }

    /**
     * @param string $filePath
     * @param string $fileLabel
     * @param string $fileDesc
     * @param string $operatingLicenseUuid
     * @return string
     */
    public function handleFileCopy(string $filePath, string $fileLabel, string $fileDesc, string $operatingLicenseUuid): string
    {
        if (empty($fileUuid = $this->fileService->handleFileCopy($filePath, $fileLabel, $fileDesc, $this->moduleBrand))) {
            return '';
        }
        return $this->insertFileOperatingLicenseRel($fileUuid, $operatingLicenseUuid);
    }

    protected function insertFileOperatingLicenseRel(string $fileUuid, string $operatingLicenseUuid, bool $operatingLicenseSigned = false): string
    {
        if (empty($fileOperatingLicenseRelUuid = $this->fileOperatingLicenseRelTable->insertFileOperatingLicenseRel($fileUuid, $operatingLicenseUuid, $operatingLicenseSigned))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileOperatingLicenseRelUuid;
    }

    public function deleteFile(string $fileOperatingLicenseRelUuid): bool
    {
        $connection = $this->beginTransaction($this->fileOperatingLicenseRelTable);

        $fileRel = $this->fileOperatingLicenseRelTable->getFileOperatingLicenseRel($fileOperatingLicenseRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileOperatingLicenseRelTable->deleteFile($fileOperatingLicenseRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    /**
     * @param string $operatingLicenseUuid
     * @param bool $all Each row has the field 'file_uuid_signed'. This is the only field they can be different for one $operatingLicenseUuid. Set $all=true shows each row.
     * @return array All files for `$operatingLicenseUuid`.
     */
    public function getViewOperatingLicenseFiles(string $operatingLicenseUuid, bool $all = false): array
    {
        $olfs = $this->viewOperatingLicenseFileTable->getViewOperatingLicenseFiles($operatingLicenseUuid, $all);
        foreach ($olfs as &$olf) {
            $olf['operating_license_number_complete'] = UniqueNumberProvider::computeNumberComplete($olf['operating_license_date'], $olf['operating_license_number_prefix'], $olf['operating_license_number']);
        }
        return $olfs;
    }

    /**
     * @param string $orderUuid
     * @return array From database view `view_operating_license_file`
     */
    public function getViewOperatingLicenseFilesForOrder(string $orderUuid): array
    {
        return $this->viewOperatingLicenseFileTable->getViewOperatingLicenseFilesForOrder($orderUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From database view `view_operating_license_file_order`
     */
    public function getViewOperatingLicenseFilesOrder(string $orderUuid): array
    {
        return $this->viewOperatingLicenseFileOrderTable->getViewOperatingLicenseFilesOrder($orderUuid);
    }

    /**
     * @param string $stockinUuid
     * @return array From database view `view_operating_license_file_stock`
     */
    public function getViewOperatingLicenseFilesStock(string $stockinUuid): array
    {
        return $this->viewOperatingLicenseFileStockTable->getViewOperatingLicenseFileStock($stockinUuid);
    }

    /**
     * @param string $factoryorderUuid
     * @return array From database view `view_operating_license_file_stock`
     */
    public function getViewOperatingLicenseFilesStockForFactoryorder(string $factoryorderUuid): array
    {
        return $this->viewOperatingLicenseFileStockTable->getViewOperatingLicenseFileStockForFactoryorder($factoryorderUuid);
    }
}

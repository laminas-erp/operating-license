<?php

namespace Lerp\OperatingLicense\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;

class OperatingLicenseAccumulateService extends AbstractService
{
    protected OperatingLicenseTable $operatingLicenseTable;
    protected OperatingLicenseService $operatingLicenseService;
    protected OperatingLicenseItemService $operatingLicenseItemService;

    public function setOperatingLicenseTable(OperatingLicenseTable $operatingLicenseTable): void
    {
        $this->operatingLicenseTable = $operatingLicenseTable;
    }

    public function setOperatingLicenseService(OperatingLicenseService $operatingLicenseService): void
    {
        $this->operatingLicenseService = $operatingLicenseService;
    }

    public function setOperatingLicenseItemService(OperatingLicenseItemService $operatingLicenseItemService): void
    {
        $this->operatingLicenseItemService = $operatingLicenseItemService;
    }

    /**
     * @param ViewOperatingLicenseItemEntity $entity
     * @return string The new created `operating_license_item_uuid`.
     */
    public function insertAndCreateOperatingLicense(ViewOperatingLicenseItemEntity $entity): string
    {
        if (empty($operatingLicenseItemUuid = $this->operatingLicenseItemService->insertViewOperatingLicenseItem($entity))) {
            return '';
        }
        if (!$this->operatingLicenseService->createOperatingLicense($operatingLicenseItemUuid, false)) {
            return '';
        }
        return $operatingLicenseItemUuid;
    }
}

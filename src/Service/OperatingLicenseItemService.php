<?php

namespace Lerp\OperatingLicense\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\OperatingLicense\Entity\OperatingLicenseEntity;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Unique\UniqueNumberProvider;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Stock\Service\StockService;

class OperatingLicenseItemService extends AbstractService
{
    use OperatingLicenseServiceTrait;

    protected OrderItemService $orderItemService;
    protected StockService $stockService;

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    /**
     * @param string $orderItemUuid
     * @return array Data from 'view_operating_license_item' plus computed field 'is_complete_for_document'. ORDERed BY operating_license_time_create DESC
     */
    public function getViewOperatingLicenseItemsForOrderItem(string $orderItemUuid): array
    {
        if (empty($olis = $this->viewOperatingLicenseItemTable->getViewOperatingLicenseItemsForOrderItem($orderItemUuid))) {
            return [];
        }
        foreach ($olis as &$oli) {
            $entity = new ViewOperatingLicenseItemEntity();
            $entity->exchangeArrayFromDatabase($oli);
            $oli['is_complete_for_document'] = $entity->isCompleteForDocument();
            $oli['operating_license_number_complete'] = UniqueNumberProvider::computeNumberComplete($oli['operating_license_date'], $oli['operating_license_number_prefix'], $oli['operating_license_number']);
        }
        return $olis;
    }

    /**
     * INSERTs INTO `operating_license` and `operating_license_item`.
     *
     * @param ViewOperatingLicenseItemEntity $entity
     * @return string The new generated `operating_license_item_uuid`.
     */
    public function insertViewOperatingLicenseItem(ViewOperatingLicenseItemEntity $entity): string
    {
        if (empty($operatingLicenseUuid = $this->operatingLicenseTable->insertOperatingLicense($entity))) {
            return '';
        }
        $entity->setOperatingLicenseUuid($operatingLicenseUuid);
        return $this->operatingLicenseItemTable->insertOperatingLicenseItem($entity);
    }

    /**
     * @param ViewOperatingLicenseItemEntity $entity
     * @return bool
     */
    public function updateViewOperatingLicenseItem(ViewOperatingLicenseItemEntity $entity): bool
    {
        return $this->operatingLicenseTable->updateOperatingLicense($entity) && $this->operatingLicenseItemTable->updateOperatingLicenseItem($entity);
    }

    /**
     * @param ParamsOperatingLicenseItemSearchEntity $params
     * @return array
     */
    public function searchOperatingLicenseItem(ParamsOperatingLicenseItemSearchEntity $params): array
    {
        $result = [];
        $count = 0;
        switch ($params->getOperatingLicenseOrigin()) {
            case 'maintenance':
                $result = $this->viewOperatingLicenseItemOrderTable->searchViewOperatingLicenseItem($params);
                $params->setDoCount(true);
                $count = $this->viewOperatingLicenseItemOrderTable->searchViewOperatingLicenseItem($params);
                break;
            case 'production':
                $result = $this->viewOperatingLicenseItemStockTable->searchViewOperatingLicenseItem($params);
                $params->setDoCount(true);
                $count = $this->viewOperatingLicenseItemStockTable->searchViewOperatingLicenseItem($params);
                break;
        }
        foreach ($result as &$item) {
            $item['operating_license_number_complete'] = UniqueNumberProvider::computeNumberComplete($item['operating_license_date'], $item['operating_license_number_prefix'], $item['operating_license_number']);
        }
        return ['result' => $result, 'count' => (!empty($count['count_ol']) ? $count['count_ol'] : 0)];
    }
}

<?php

namespace Lerp\OperatingLicense\Controller\Rest\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\OperatingLicense\Form\Files\FileOperatingLicenseForm;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;

class FileOperatingLicenseRestController extends AbstractUserRestController
{
    protected int $fileCategoryIdCompute;
    protected int $fileCategoryIdSigned;
    protected FileOperatingLicenseForm $fileOperatingLicenseForm;
    protected FileOperatingLicenseRelService $fileOperatingLicenseRelService;

    public function setFileCategoryIdCompute(int $fileCategoryIdCompute): void
    {
        $this->fileCategoryIdCompute = $fileCategoryIdCompute;
    }

    public function setFileCategoryIdSigned(int $fileCategoryIdSigned): void
    {
        $this->fileCategoryIdSigned = $fileCategoryIdSigned;
    }

    public function setFileOperatingLicenseForm(FileOperatingLicenseForm $fileOperatingLicenseForm): void
    {
        $this->fileOperatingLicenseForm = $fileOperatingLicenseForm;
    }

    public function setFileOperatingLicenseRelService(FileOperatingLicenseRelService $fileOperatingLicenseRelService): void
    {
        $this->fileOperatingLicenseRelService = $fileOperatingLicenseRelService;
    }

    /**
     * UPLOAD a file that belongs to a operating_license.
     * This file will be a signed operating license (file_operating_license_rel_signed=TRUE).
     * For one operating_license_uuid it can be max two files: db.file_operating_license_rel.file_operating_license_rel_signed FALSE | TRUE
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $operatingLicenseUuid = $data['operating_license_uuid'];
        if (!(new Uuid())->isValid($operatingLicenseUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->fileOperatingLicenseForm->getFileFieldset()->setFileInputAvailable(true);
        $this->fileOperatingLicenseForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->fileOperatingLicenseForm->setData($post);
            if ($this->fileOperatingLicenseForm->isValid()) {
                $formData = $this->fileOperatingLicenseForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                $fileEntity->setFileCategoryId($this->fileCategoryIdSigned);
                if (!empty($fileUuid = $this->fileOperatingLicenseRelService->handleFileUpload($fileEntity, $operatingLicenseUuid, true))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->fileOperatingLicenseForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

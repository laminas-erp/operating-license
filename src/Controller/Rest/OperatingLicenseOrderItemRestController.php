<?php

namespace Lerp\OperatingLicense\Controller\Rest;

use Bitkorn\Trinket\Service\LangService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Bitkorn\User\Entity\User\Rightsnroles;
use Laminas\Http\Response;
use Laminas\Mvc\I18n\Translator;
use Laminas\Validator\Uuid;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseOrderItemRestController extends AbstractUserRestController
{
    protected OperatingLicenseItemService $operatingLicenseItemService;
    protected OperatingLicenseService $operatingLicenseService;
    protected ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm;
    protected Translator $translator;

    public function setOperatingLicenseItemService(OperatingLicenseItemService $operatingLicenseItemService): void
    {
        $this->operatingLicenseItemService = $operatingLicenseItemService;
    }

    public function setOperatingLicenseService(OperatingLicenseService $operatingLicenseService): void
    {
        $this->operatingLicenseService = $operatingLicenseService;
    }

    public function setViewOperatingLicenseItemForm(ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm): void
    {
        $this->viewOperatingLicenseItemForm = $viewOperatingLicenseItemForm;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * Get data from 'db.view_operating_license_item' plus computed field 'is_complete_for_document'.
     * ORDERed BY operating_license_time_create DESC
     *
     * @param string $id order_item_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles(Rightsnroles::METHOD_GETP)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->operatingLicenseItemService->getViewOperatingLicenseItemsForOrderItem($id));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Insert data for an operating license (db.operating_license_item & db.operating_license).
     *
     * @param array $data With key 'order_item_uuid'.
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles(Rightsnroles::METHOD_POST)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->viewOperatingLicenseItemForm->setOperatingLicenseSourceType(OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM);
        $this->viewOperatingLicenseItemForm->setSecondaryKeysAvailable(true);
        $this->viewOperatingLicenseItemForm->init();
        $this->viewOperatingLicenseItemForm->setData($data);
        if (!$this->viewOperatingLicenseItemForm->isValid()) {
            $jsonModel->addMessages($this->viewOperatingLicenseItemForm->getMessages());
            return $jsonModel;
        }
        $entity = new ViewOperatingLicenseItemEntity();
        if (!$entity->exchangeArrayFromDatabase($this->viewOperatingLicenseItemForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($operatingLicenseUuid = $this->operatingLicenseItemService->insertViewOperatingLicenseItem($entity))) {
            $jsonModel->setUuid($operatingLicenseUuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * Update a db.operating_license_item and a db.operating_license dataset.
     * ONLY if no PDF document exist (operating_license.operating_license_number is not set).
     *
     * @param string $id operating_license_item_uuid !?!?!?
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkRightsnroles(Rightsnroles::METHOD_UPDATE)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->viewOperatingLicenseItemForm->setOperatingLicenseSourceType(OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM);
        $this->viewOperatingLicenseItemForm->setPrimaryKeyAvailable(true);
        $this->viewOperatingLicenseItemForm->init();
        $this->viewOperatingLicenseItemForm->setData($data);
        if (!$this->viewOperatingLicenseItemForm->isValid()) {
            $jsonModel->addMessages($this->viewOperatingLicenseItemForm->getMessages());
            return $jsonModel;
        }
        $entity = new ViewOperatingLicenseItemEntity();
        if (!$entity->exchangeArrayFromDatabase($this->viewOperatingLicenseItemForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->operatingLicenseService->isOperatingLicenseNumber($entity->getOperatingLicenseUuid())) {
            $jsonModel->addMessage($this->translator->translate('operating license doc exist', 'lerp_operatinglicense', $this->langService->getLocale()));
        }
        if ($this->operatingLicenseItemService->updateViewOperatingLicenseItem($entity)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

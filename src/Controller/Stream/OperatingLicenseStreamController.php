<?php

namespace Lerp\OperatingLicense\Controller\Stream;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Tools\Render\RenderTool;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class OperatingLicenseStreamController extends AbstractUserController
{
    protected FileService $fileService;

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @return JsonModel
     */
    public function streamAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('sessionhash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $fileUuid = $this->params('id');
        if (!(new Uuid())->isValid($fileUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (
            empty($fileForOutput = $this->fileService->getFileForOutput($fileUuid))
            || empty($fileForOutput['fqfn'])
            || !is_readable($fileForOutput['fqfn'])
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        RenderTool::outputAttachment(
            $fileForOutput['fqfn']
            , $fileForOutput['file_filename_label']
            , $fileForOutput['file_mimetype']
            , $this->params()->fromQuery('cdispo', '')
        );
        return $jsonModel;
    }
}

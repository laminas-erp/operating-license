<?php

namespace Lerp\OperatingLicense\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseSearchAjaxController extends AbstractUserController
{
    protected ParamsOperatingLicenseItemSearchEntity $paramsOperatingLicenseItemSearchEntity;
    protected OperatingLicenseItemService $operatingLicenseItemService;

    public function setParamsOperatingLicenseItemSearchEntity(ParamsOperatingLicenseItemSearchEntity $paramsOperatingLicenseItemSearchEntity): void
    {
        $this->paramsOperatingLicenseItemSearchEntity = $paramsOperatingLicenseItemSearchEntity;
    }

    public function setOperatingLicenseItemService(OperatingLicenseItemService $operatingLicenseItemService): void
    {
        $this->operatingLicenseItemService = $operatingLicenseItemService;
    }

    /**
     * @return JsonModel
     */
    public function searchOperatingLicenseItemOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $this->paramsOperatingLicenseItemSearchEntity->setFromParamsArray($this->getRequest()->getPost()->toArray());
        $this->paramsOperatingLicenseItemSearchEntity->setOperatingLicenseOrigin(OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM);
        $res = $this->operatingLicenseItemService->searchOperatingLicenseItem($this->paramsOperatingLicenseItemSearchEntity);
        if (empty($res) || count($res) != 2) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setArr($res['result']);
        $jsonModel->setCount($res['count']);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function searchOperatingLicenseItemStockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $this->paramsOperatingLicenseItemSearchEntity->setFromParamsArray($this->getRequest()->getPost()->toArray());
        $this->paramsOperatingLicenseItemSearchEntity->setOperatingLicenseOrigin(OperatingLicenseService::SOURCE_TYPE_STOCKIN);
        $res = $this->operatingLicenseItemService->searchOperatingLicenseItem($this->paramsOperatingLicenseItemSearchEntity);
        if (empty($res) || count($res) != 2) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setArr($res['result']);
        $jsonModel->setCount($res['count']);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

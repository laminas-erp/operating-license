<?php

namespace Lerp\OperatingLicense\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseCreateAjaxController extends AbstractUserController
{
    protected OperatingLicenseItemService $operatingLicenseItemService;
    protected OperatingLicenseService $operatingLicenseService;
    protected ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm;
    protected OperatingLicenseAccumulateService $operatingLicenseAccumulateService;

    public function setOperatingLicenseItemService(OperatingLicenseItemService $operatingLicenseItemService): void
    {
        $this->operatingLicenseItemService = $operatingLicenseItemService;
    }

    public function setOperatingLicenseService(OperatingLicenseService $operatingLicenseService): void
    {
        $this->operatingLicenseService = $operatingLicenseService;
    }

    public function setViewOperatingLicenseItemForm(ViewOperatingLicenseItemForm $viewOperatingLicenseItemForm): void
    {
        $this->viewOperatingLicenseItemForm = $viewOperatingLicenseItemForm;
    }

    public function setOperatingLicenseAccumulateService(OperatingLicenseAccumulateService $operatingLicenseAccumulateService): void
    {
        $this->operatingLicenseAccumulateService = $operatingLicenseAccumulateService;
    }

    /**
     * @return JsonModel
     */
    public function createAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($operatingLicenseItemUuid = $this->params('operating_license_item_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->operatingLicenseService->createOperatingLicense($operatingLicenseItemUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function replaceForStockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->viewOperatingLicenseItemForm->setOperatingLicenseSourceType(OperatingLicenseService::SOURCE_TYPE_STOCKIN);
        $this->viewOperatingLicenseItemForm->setIsOperatingLicenseUuidPrev(true);
        $this->viewOperatingLicenseItemForm->setSecondaryKeysAvailable(true);
        $this->viewOperatingLicenseItemForm->init();
        $this->viewOperatingLicenseItemForm->setData($this->getRequest()->getPost()->toArray());
        if (!$this->viewOperatingLicenseItemForm->isValid()) {
            $jsonModel->addMessages($this->viewOperatingLicenseItemForm->getMessages());
            return $jsonModel;
        }
        $entity = new ViewOperatingLicenseItemEntity();
        if (!$entity->exchangeArrayFromDatabase($this->viewOperatingLicenseItemForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->operatingLicenseAccumulateService->insertAndCreateOperatingLicense($entity))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

}

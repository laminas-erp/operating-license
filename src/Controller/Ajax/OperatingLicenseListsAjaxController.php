<?php

namespace Lerp\OperatingLicense\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseListsAjaxController extends AbstractUserController
{
    protected OperatingLicenseService $operatingLicenseService;

    public function setOperatingLicenseService(OperatingLicenseService $operatingLicenseService): void
    {
        $this->operatingLicenseService = $operatingLicenseService;
    }

    /**
     * @return JsonModel
     */
    public function typesAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->operatingLicenseService->validOperatingLicenseOrigin($olOrigin = $this->params('ol_origin'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_404);
            $jsonModel->addMessage('Wrong operating license origin: ' . $olOrigin);
            return $jsonModel;
        }
        $jsonModel->setObj($this->operatingLicenseService->getOperatingLicenseTypeAssoc($olOrigin));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function statusWorksAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->operatingLicenseService->getStatusWorkAssoc());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel With values from database enum enum_operating_license_origin as assoc.
     */
    public function originsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->operatingLicenseService->getOperatingLicenseOrigins());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

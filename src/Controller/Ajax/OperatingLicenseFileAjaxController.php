<?php

namespace Lerp\OperatingLicense\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseFileAjaxController extends AbstractUserController
{
    protected OperatingLicenseService $operatingLicenseService;
    protected FileOperatingLicenseRelService $fileOperatingLicenseRelService;

    public function setOperatingLicenseService(OperatingLicenseService $operatingLicenseService): void
    {
        $this->operatingLicenseService = $operatingLicenseService;
    }

    public function setFileOperatingLicenseRelService(FileOperatingLicenseRelService $fileOperatingLicenseRelService): void
    {
        $this->fileOperatingLicenseRelService = $fileOperatingLicenseRelService;
    }

    /**
     * @return JsonModel
     */
    public function getFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($operatingLicenseUuid = $this->params('operating_license_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOperatingLicenseRelService->getViewOperatingLicenseFiles($operatingLicenseUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getOrderFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($orderUuid = $this->params('order_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOperatingLicenseRelService->getViewOperatingLicenseFilesForOrder($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getViewOperatingLicenseFilesOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($orderUuid = $this->params('order_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOperatingLicenseRelService->getViewOperatingLicenseFilesOrder($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getViewOperatingLicenseFilesStockAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($stockinUuid = $this->params('stockin_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOperatingLicenseRelService->getViewOperatingLicenseFilesStock($stockinUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getViewOperatingLicenseFilesStockFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($foUuid = $this->params('fo_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->fileOperatingLicenseRelService->getViewOperatingLicenseFilesStockForFactoryorder($foUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

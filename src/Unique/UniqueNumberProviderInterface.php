<?php

namespace Lerp\OperatingLicense\Unique;

/**
 * A Unique consists of a number and a string. Together it is one Unique.
 *
 * ...with a number and the unique (the number plus additional characters ...company brand dependent)
 */
interface UniqueNumberProviderInterface
{

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string;

    public function getPrefix(): string;

    public function getNumber(): int;

    public function getNumberComplete(): string;

    /**
     * Here you must implement the trick :)
     *
     * @param int $operatingLicenseTypeId On of the IDs in database table 'operating_license_type'.
     * @return string
     */
    public function computeGetNumberComplete(int $operatingLicenseTypeId): string;

    public function validUniqueNumber(string $uniqueNumber): bool;

    /**
     * Compute/glue/split the unique number with possible parameter.
     *
     * @param string $date
     * @param string $prefix
     * @param int $number
     * @return string
     */
    public static function computeNumberComplete(string $date, string $prefix, int $number): string;
}

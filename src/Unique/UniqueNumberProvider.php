<?php

namespace Lerp\OperatingLicense\Unique;

use Laminas\Log\Logger;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;

/**
 * @todo alles statische Zeug raus, um weitere Implementationen von UniqueNumberProviderInterface nutzen zu können.
 * ACC Columbia jet Service Implementation.
 */
class UniqueNumberProvider implements UniqueNumberProviderInterface
{
    const PAD_LENGTH = 4;
    const PAD_STRING = '0';

    const OPERATING_LICENSE_TYPE_EASA_21G = 1;
    const OPERATING_LICENSE_TYPE_EASA_145 = 2;
    const OPERATING_LICENSE_TYPE_LBA_21G = 3;
    const OPERATING_LICENSE_TYPE_LBA_145 = 4;
    const OPERATING_LICENSE_TYPE_COC = 5;

    protected static array $operatingLicenseTypes = [
        self::OPERATING_LICENSE_TYPE_EASA_21G => 'EASA 21G',
        self::OPERATING_LICENSE_TYPE_EASA_145 => 'EASA 145',
        self::OPERATING_LICENSE_TYPE_LBA_21G  => 'LBA 21G',
        self::OPERATING_LICENSE_TYPE_LBA_145  => 'LBA 145',
        self::OPERATING_LICENSE_TYPE_COC      => 'CoC',
    ];

    /**
     * self::PREFIX_LEFT . $this->yearShort . '-' . self::PREFIX_LOCATION + self::PAD_LENGTH
     */
    const UNIQUE_LENGTH_FROM = 11;
    const UNIQUE_LENGTH_TO = 13;
    protected Logger $logger;

    /**
     * Standort Nummer
     */
    const PREFIX = 'SWE';
    protected string $prefix;
    protected int $year;
    protected int $yearShort;
    protected \DateTime $dateTimeFrom; // to fetch next number in period
    protected \DateTime $dateTimeTo; // to fetch next number in period
    protected int $number;
    protected string $numberComplete;
    protected string $message;

    protected OperatingLicenseTable $operatingLicenseTable;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setOperatingLicenseTable(OperatingLicenseTable $operatingLicenseTable): void
    {
        $this->operatingLicenseTable = $operatingLicenseTable;
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $this->year = date('Y');
        $this->yearShort = substr($this->year, 2, 2);
        $this->dateTimeFrom = new \DateTime($this->year . '-01-01 00:00:00');
        $this->dateTimeTo = new \DateTime($this->year . '-12-31 23:59:59');
        $this->prefix = self::PREFIX . '-';
    }

    /**
     * @param int $operatingLicenseTypeId On of the self::OPERATING_LICENSE_TYPE_*
     * @return bool
     */
    protected function generate(int $operatingLicenseTypeId): bool
    {
        if (!array_key_exists($operatingLicenseTypeId, self::$operatingLicenseTypes)) {
            $this->message = 'Operating License Type is not supported.';
            return false;
        }
        if (!isset($this->dateTimeFrom) || !isset($this->dateTimeTo)) {
            $this->message = 'Dates are not set. Perhaps you forgot to call the init() method!?';
            return false;
        }
        if (($this->number = $this->operatingLicenseTable->getMaxNumberInPeriod($this->dateTimeFrom, $this->dateTimeTo)) < 0) {
            $this->message = 'Error while compute max-order number';
            return false;
        }
        if ($operatingLicenseTypeId == self::OPERATING_LICENSE_TYPE_EASA_21G || $operatingLicenseTypeId == self::OPERATING_LICENSE_TYPE_LBA_21G) {
            $this->prefix .= 'P-';
        }
        $this->number++;
        return $this->computeGlue();
    }

    protected function computeGlue(): bool
    {
        if (empty($this->prefix) || empty($this->yearShort) || empty($this->number)) {
            return false;
        }
        $this->numberComplete = $this->prefix . $this->yearShort . '-'
            . str_pad($this->number, self::PAD_LENGTH, self::PAD_STRING, STR_PAD_LEFT);
        $len = strlen($this->numberComplete);
        return $len >= self::UNIQUE_LENGTH_FROM && $len <= self::UNIQUE_LENGTH_TO;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getNumberComplete(): string
    {
        return $this->numberComplete;
    }

    /**
     * @param int $operatingLicenseTypeId On of the self::OPERATING_LICENSE_TYPE_*
     * @return string
     */
    public function computeGetNumberComplete(int $operatingLicenseTypeId): string
    {
        try {
            $this->init();
        } catch (\Exception $e) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() ' . $e->getMessage());
            $this->message = 'Error while init()';
            return '';
        }
        if (!$this->generate($operatingLicenseTypeId)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
            $this->logger->err('message: ' . $this->message);
            return '';
        }
        return $this->numberComplete;
    }

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    public function validUniqueNumber(string $uniqueNumber): bool
    {
        return preg_match('/^[A-Z]{3}(P-)?\d{2}-\d{' . (self::PAD_LENGTH + 1) . '}$/', $uniqueNumber) === 1;
    }

    public static function computeNumberComplete(string $date, string $prefix, int $number): string
    {
        if(empty($date) || empty($prefix) || empty($number)) {
            return '';
        }
        return $prefix . substr($date, 0, 4) . '-' . str_pad($number, 4, STR_PAD_LEFT, '0');
    }
}

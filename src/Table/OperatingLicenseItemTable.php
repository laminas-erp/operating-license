<?php

namespace Lerp\OperatingLicense\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Lerp\OperatingLicense\Entity\ViewOperatingLicenseItemEntity;

class OperatingLicenseItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'operating_license_item';

    /**
     * @param string $operatingLicenseItemUuid
     * @return array
     */
    public function getOperatingLicenseItem(string $operatingLicenseItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['operating_license_item_uuid' => $operatingLicenseItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param ViewOperatingLicenseItemEntity $operatingLicenseItemEntity
     * @return string
     */
    public function insertOperatingLicenseItem(ViewOperatingLicenseItemEntity $operatingLicenseItemEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'operating_license_item_uuid'          => $uuid,
                'operating_license_uuid'               => $operatingLicenseItemEntity->getOperatingLicenseUuid(),
                'operating_license_item_position'      => $operatingLicenseItemEntity->getOperatingLicenseItemPosition() ?: 1,
                'operating_license_item_desc'          => $operatingLicenseItemEntity->getOperatingLicenseItemDesc(),
                'operating_license_item_part_number'   => $operatingLicenseItemEntity->getOperatingLicenseItemPartNumber(),
                'operating_license_item_quantity'      => $operatingLicenseItemEntity->getOperatingLicenseItemQuantity(),
                'operating_license_item_serial_number' => $operatingLicenseItemEntity->getOperatingLicenseItemSerialNumber(),
                'operating_license_item_status_work'   => $operatingLicenseItemEntity->getOperatingLicenseItemStatusWork(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateOperatingLicenseItem(ViewOperatingLicenseItemEntity $operatingLicenseItemEntity): bool
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'operating_license_item_position'      => $operatingLicenseItemEntity->getOperatingLicenseItemPosition() ?: 1,
                'operating_license_item_desc'          => $operatingLicenseItemEntity->getOperatingLicenseItemDesc(),
                'operating_license_item_part_number'   => $operatingLicenseItemEntity->getOperatingLicenseItemPartNumber(),
                'operating_license_item_quantity'      => $operatingLicenseItemEntity->getOperatingLicenseItemQuantity(),
                'operating_license_item_serial_number' => $operatingLicenseItemEntity->getOperatingLicenseItemSerialNumber(),
                'operating_license_item_status_work'   => $operatingLicenseItemEntity->getOperatingLicenseItemStatusWork(),
            ]);
            $update->where(['operating_license_item_uuid' => $operatingLicenseItemEntity->getOperatingLicenseItemUuid()]);
            return $this->updateWith($update) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }
}

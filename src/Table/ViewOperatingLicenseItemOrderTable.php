<?php

namespace Lerp\OperatingLicense\Table;

class ViewOperatingLicenseItemOrderTable extends ViewOperatingLicenseItemTable
{
    /** @var string */
    protected $table = 'view_operating_license_item_order';
}

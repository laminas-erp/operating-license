<?php

namespace Lerp\OperatingLicense\Table;

class ViewOperatingLicenseItemStockTable extends ViewOperatingLicenseItemTable
{
    /** @var string */
    protected $table = 'view_operating_license_item_stock';
}

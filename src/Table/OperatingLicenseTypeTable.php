<?php

namespace Lerp\OperatingLicense\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class OperatingLicenseTypeTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'operating_license_type';

    /**
     * @param string $origin One of the values in database ENUM enum_operating_license_origin
     * @return array
     */
    public function getOperatingLicenseTypeAssoc(string $origin = ''): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            if ($origin) {
                $select->where(['operating_license_type_origin' => $origin]);
            }
            $select->order('operating_license_type_id');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                do {
                    $current = $result->current();
                    $assoc[$current['operating_license_type_id']] = $current['operating_license_type_name'];
                    $result->next();
                } while ($result->valid());
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}

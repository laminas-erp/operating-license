<?php

namespace Lerp\OperatingLicense\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;

class ViewOperatingLicenseItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_operating_license_item';

    /**
     * @param string $operatingLicenseItemUuid
     * @return array
     */
    public function getViewOperatingLicenseItem(string $operatingLicenseItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['operating_license_item_uuid' => $operatingLicenseItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $orderItemUuid
     * @return array ORDERed BY operating_license_time_create DESC
     */
    public function getViewOperatingLicenseItemsForOrderItem(string $orderItemUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_item_uuid' => $orderItemUuid]);
            $select->order('operating_license_time_create DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param ParamsOperatingLicenseItemSearchEntity $params
     * @return array If doCount then ['count_ol' => 666]
     */
    public function searchViewOperatingLicenseItem(ParamsOperatingLicenseItemSearchEntity $params): array
    {
        $select = $this->sql->select();
        try {
            if ($this->table == 'view_operating_license_item') {
                $params->setProductNo('');
            }
            if ($params->isDoCount()) {
                $select->columns(['count_ol' => new Expression('COUNT(*)')]);
            } else {
                $params->computeSelect($select);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($params->isDoCount()) {
                    return $result->toArray()[0];
                } else {
                    return $result->toArray();
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

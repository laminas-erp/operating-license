<?php

namespace Lerp\OperatingLicense\Table\Files;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewOperatingLicenseFileStockTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_operating_license_file_stock';

    /**
     * @param string $stockinUuid
     * @return array
     */
    public function getViewOperatingLicenseFileStock(string $stockinUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['stockin_uuid' => $stockinUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $factoryorderUuid
     * @return array
     */
    public function getViewOperatingLicenseFileStockForFactoryorder(string $factoryorderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['factoryorder_uuid' => $factoryorderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

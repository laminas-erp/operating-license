<?php

namespace Lerp\OperatingLicense\Table\Files;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewOperatingLicenseFileTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_operating_license_file';

    /**
     * @param string $fileOperatingLicenseRelUuid
     * @return array
     */
    public function getViewOperatingLicenseFile(string $fileOperatingLicenseRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_operating_license_rel_uuid' => $fileOperatingLicenseRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $operatingLicenseUuid
     * @param bool $all Each row has the field 'file_uuid_signed'. This is the only field they can be different for one $operatingLicenseUuid. Set $all=true shows each row.
     * @return array
     */
    public function getViewOperatingLicenseFiles(string $operatingLicenseUuid, bool $all = false): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['operating_license_uuid' => $operatingLicenseUuid]);
            if (!$all) {
                $select->where(['file_operating_license_rel_signed' => false]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getViewOperatingLicenseFilesForOrder(string $orderUuid, bool $onlySigned = true): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_uuid' => $orderUuid]);
            if ($onlySigned) {
                $select->where(['file_operating_license_rel_signed' => true]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

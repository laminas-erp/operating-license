<?php

namespace Lerp\OperatingLicense\Table\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FileOperatingLicenseRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_operating_license_rel';

    /**
     * @param string $fileOperatingLicenseRelUuid
     * @return array
     */
    public function getFileOperatingLicenseRel(string $fileOperatingLicenseRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_operating_license_rel_uuid' => $fileOperatingLicenseRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOperatingLicenseRel(string $fileOperatingLicenseRelUuid): bool
    {
        return !empty($this->getFileOperatingLicenseRel($fileOperatingLicenseRelUuid));
    }

    /**
     * @param string $fileOperatingLicenseRelUuid
     * @return array
     */
    public function getFileOperatingLicenseRelJoined(string $fileOperatingLicenseRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_operating_license_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_operating_license_rel_uuid' => $fileOperatingLicenseRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileOperatingLicenseRelForFileAndOperatingLicense(string $fileUuid, string $operatingLicenseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'operating_license_uuid' => $operatingLicenseUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileOperatingLicenseRelForFileAndOperatingLicense(string $fileUuid, string $operatingLicenseUuid): bool
    {
        $file = $this->getFileOperatingLicenseRelForFileAndOperatingLicense($fileUuid, $operatingLicenseUuid);
        return !empty($file);
    }

    public function getFilesForOperatingLicense(string $operatingLicenseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_operating_license_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['operating_license_uuid' => $operatingLicenseUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileOperatingLicenseRel(string $fileUuid, string $operatingLicenseUuid, bool $operatingLicenseSigned = false): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_operating_license_rel_uuid'   => $uuid,
                'file_uuid'                         => $fileUuid,
                'operating_license_uuid'            => $operatingLicenseUuid,
                'file_operating_license_rel_signed' => $operatingLicenseSigned,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * The foreign key on file_uuid has `ON DELETE CASCADE`.
     *
     * @param string $fileOperatingLicenseRelUuid
     * @return int
     */
    public function deleteFile(string $fileOperatingLicenseRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_operating_license_rel_uuid' => $fileOperatingLicenseRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}

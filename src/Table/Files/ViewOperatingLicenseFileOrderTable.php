<?php

namespace Lerp\OperatingLicense\Table\Files;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewOperatingLicenseFileOrderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_operating_license_file_order';

    /**
     * @param string $orderUuid
     * @return array
     */
    public function getViewOperatingLicenseFilesOrder(string $orderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['order_uuid' => $orderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

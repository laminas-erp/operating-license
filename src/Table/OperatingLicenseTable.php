<?php

namespace Lerp\OperatingLicense\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Lerp\OperatingLicense\Entity\OperatingLicenseEntity;
use Lerp\OperatingLicense\Unique\UniqueNumberProvider;

class OperatingLicenseTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'operating_license';

    /**
     * @param string $operatingLicenseUuid
     * @return array
     */
    public function getOperatingLicense(string $operatingLicenseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['operating_license_uuid' => $operatingLicenseUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existOperatingLicense(string $operatingLicenseUuid): bool
    {
        return !empty($this->getOperatingLicense($operatingLicenseUuid));
    }

    /**
     * Insert data for an operating license without operating license number.
     *
     * @param OperatingLicenseEntity $operatingLicenseEntity
     * @return string
     */
    public function insertOperatingLicense(OperatingLicenseEntity $operatingLicenseEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'operating_license_uuid'        => $uuid,
                'operating_license_type_id'     => $operatingLicenseEntity->getOperatingLicenseTypeId(),
                'order_uuid'                    => $operatingLicenseEntity->getOrderUuid() ?: null,
                'order_item_uuid'               => $operatingLicenseEntity->getOrderItemUuid() ?: null,
                'stockin_uuid'                  => $operatingLicenseEntity->getStockinUuid() ?: null,
                'operating_license_date'        => $operatingLicenseEntity->getOperatingLicenseDate(),
                'operating_license_work_number' => $operatingLicenseEntity->getOperatingLicenseWorkNumber(),
                'operating_license_remark'      => $operatingLicenseEntity->getOperatingLicenseRemark(),
                'user_uuid_certified_staff'     => $operatingLicenseEntity->getUserUuidCertifiedStaff(),
                'operating_license_uuid_prev'   => $operatingLicenseEntity->getOperatingLicenseUuidPrev() ?: null,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateOperatingLicense(OperatingLicenseEntity $operatingLicenseEntity): bool
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'operating_license_date'        => $operatingLicenseEntity->getOperatingLicenseDate(),
                'operating_license_work_number' => $operatingLicenseEntity->getOperatingLicenseWorkNumber(),
                'operating_license_remark'      => $operatingLicenseEntity->getOperatingLicenseRemark(),
                'user_uuid_certified_staff'     => $operatingLicenseEntity->getUserUuidCertifiedStaff(),
            ]);
            $update->where(['operating_license_uuid' => $operatingLicenseEntity->getOperatingLicenseUuid()]);
            return $this->updateWith($update) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getMaxNumberInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(operating_license_number)')]);
            $select->where->greaterThanOrEqualTo('operating_license_date', $dateTimeFrom->format('Y-m-d'));
            $select->where->lessThanOrEqualTo('operating_license_date', $dateTimeTo->format('Y-m-d'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * The only function with which you can update the OperatingLicenseNumber.
     *
     * @param OperatingLicenseEntity $operatingLicenseEntity
     * @return bool
     */
    public function updateOperatingLicenseNumber(OperatingLicenseEntity $operatingLicenseEntity): bool
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'operating_license_number'          => $operatingLicenseEntity->getOperatingLicenseNumber(),
                'operating_license_number_prefix'   => $operatingLicenseEntity->getOperatingLicenseNumberPrefix(),
                'operating_license_number_complete' => $operatingLicenseEntity->getOperatingLicenseNumberComplete(),
            ]);
            $update->where(['operating_license_uuid' => $operatingLicenseEntity->getOperatingLicenseUuid()]);
            return $this->updateWith($update) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }
}

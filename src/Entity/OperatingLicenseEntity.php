<?php

namespace Lerp\OperatingLicense\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class OperatingLicenseEntity extends AbstractEntity
{
    protected array $mapping = [
        'operating_license_uuid'            => 'operating_license_uuid',
        'operating_license_type_id'         => 'operating_license_type_id',
        'order_uuid'                        => 'order_uuid',
        'order_item_uuid'                   => 'order_item_uuid',
        'stockin_uuid'                      => 'stockin_uuid',
        'operating_license_time_create'     => 'operating_license_time_create',
        'operating_license_date'            => 'operating_license_date',
        'operating_license_number'          => 'operating_license_number',
        'operating_license_number_prefix'   => 'operating_license_number_prefix',
        'operating_license_number_complete' => 'operating_license_number_complete',
        'operating_license_work_number'     => 'operating_license_work_number',
        'operating_license_remark'          => 'operating_license_remark',
        'user_uuid_certified_staff'         => 'user_uuid_certified_staff',
        'operating_license_uuid_prev'       => 'operating_license_uuid_prev',
    ];

    protected $primaryKey = 'operating_license_uuid';

    public function getOperatingLicenseUuid(): string
    {
        if (!isset($this->storage['operating_license_uuid'])) {
            return '';
        }
        return $this->storage['operating_license_uuid'];
    }

    public function setOperatingLicenseUuid(string $operatingLicenseUuid): void
    {
        $this->storage['operating_license_uuid'] = $operatingLicenseUuid;
    }

    public function getOperatingLicenseTypeId(): int
    {
        if (!isset($this->storage['operating_license_type_id'])) {
            return 0;
        }
        return $this->storage['operating_license_type_id'];
    }

    public function setOperatingLicenseTypeId(int $operatingLicenseTypeId): void
    {
        $this->storage['operating_license_type_id'] = $operatingLicenseTypeId;
    }

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getOrderItemUuid(): string
    {
        if (!isset($this->storage['order_item_uuid'])) {
            return '';
        }
        return $this->storage['order_item_uuid'];
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->storage['order_item_uuid'] = $orderItemUuid;
    }

    public function getStockinUuid(): string
    {
        if (!isset($this->storage['stockin_uuid'])) {
            return '';
        }
        return $this->storage['stockin_uuid'];
    }

    public function setStockinUuid(string $stockinUuid): void
    {
        $this->storage['stockin_uuid'] = $stockinUuid;
    }

    public function getOperatingLicenseDate(): string
    {
        if (!isset($this->storage['operating_license_date'])) {
            return '';
        }
        return $this->storage['operating_license_date'];
    }

    public function setOperatingLicenseDate(string $operatingLicenseDate): void
    {
        $this->storage['operating_license_date'] = $operatingLicenseDate;
    }

    public function getOperatingLicenseNumber(): int
    {
        if (!isset($this->storage['operating_license_number'])) {
            return 0;
        }
        return $this->storage['operating_license_number'];
    }

    public function setOperatingLicenseNumber(int $operatingLicenseNumber): void
    {
        $this->storage['operating_license_number'] = $operatingLicenseNumber;
    }

    public function getOperatingLicenseNumberPrefix(): string
    {
        if (!isset($this->storage['operating_license_number_prefix'])) {
            return '';
        }
        return $this->storage['operating_license_number_prefix'];
    }

    public function setOperatingLicenseNumberPrefix(string $operatingLicenseNumberPrefix): void
    {
        $this->storage['operating_license_number_prefix'] = $operatingLicenseNumberPrefix;
    }

    public function getOperatingLicenseNumberComplete(): string
    {
        if (!isset($this->storage['operating_license_number_complete'])) {
            return '';
        }
        return $this->storage['operating_license_number_complete'];
    }

    public function setOperatingLicenseNumberComplete(string $operatingLicenseNumberComplete): void
    {
        $this->storage['operating_license_number_complete'] = $operatingLicenseNumberComplete;
    }

    public function getOperatingLicenseWorkNumber(): string
    {
        if (!isset($this->storage['operating_license_work_number'])) {
            return '';
        }
        return $this->storage['operating_license_work_number'];
    }

    public function setOperatingLicenseWorkNumber(string $operatingLicenseWorkNumber): void
    {
        $this->storage['operating_license_work_number'] = $operatingLicenseWorkNumber;
    }

    public function getOperatingLicenseRemark(): string
    {
        if (!isset($this->storage['operating_license_remark'])) {
            return '';
        }
        return $this->storage['operating_license_remark'];
    }

    public function setOperatingLicenseRemark(string $operatingLicenseRemark): void
    {
        $this->storage['operating_license_remark'] = $operatingLicenseRemark;
    }

    public function getUserUuidCertifiedStaff(): string
    {
        if (!isset($this->storage['user_uuid_certified_staff'])) {
            return '';
        }
        return $this->storage['user_uuid_certified_staff'];
    }

    public function setUserUuidCertifiedStaff(string $userUuidCertifiedStaff): void
    {
        $this->storage['user_uuid_certified_staff'] = $userUuidCertifiedStaff;
    }

    public function getOperatingLicenseUuidPrev(): string
    {
        if (!isset($this->storage['operating_license_uuid_prev'])) {
            return '';
        }
        return $this->storage['operating_license_uuid_prev'];
    }

    public function setOperatingLicenseUuidPrev(string $operatingLicenseUuidPrev): void
    {
        $this->storage['operating_license_uuid_prev'] = $operatingLicenseUuidPrev;
    }
}

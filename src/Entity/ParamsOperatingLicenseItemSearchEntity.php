<?php

namespace Lerp\OperatingLicense\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Db\Sql\Select;
use Laminas\Log\Logger;
use Laminas\Validator\Date;

class ParamsOperatingLicenseItemSearchEntity extends ParamsBase
{
    protected Logger $logger;
    protected Date $date;
    protected FilterChainStringSanitize $stringFilter;
    protected array $orderFieldsAvailable = [
        'operating_license_number_complete',
        'operating_license_work_number',
        'operating_license_item_desc',
        'operating_license_item_part_number',
        'operating_license_item_serial_number',
        'operating_license_item_status_work',
        'user_details_name_last',
        'operating_license_date',
        'operating_license_type_name',
        'product_no_no',
    ];
    protected array $operatingLicenseTypeIdAssoc;
    protected array $operatingLicenseItemStatusWorkAssoc;
    protected array $operatingLicenseOrigins;

    /**
     * @var string From database enum enum_operating_license_origin
     */
    protected string $operatingLicenseOrigin = '';

    protected string $operatingLicenseDateFrom = '';
    protected string $operatingLicenseDateTo = '';
    protected string $operatingLicenseNumber;
    protected string $operatingLicenseWorkNumber;
    protected string $operatingLicenseRemark;
    protected string $userUuidCertifiedStaff = '';
    protected int $operatingLicenseTypeId = 0;
    protected string $operatingLicenseItemDesc;
    protected string $operatingLicenseItemPartNumber;
    protected string $operatingLicenseItemSerialNumber;
    protected string $operatingLicenseItemStatusWork = '';
    protected string $productNo;

    public function __construct()
    {
        parent::__construct();
        $this->date = new Date();
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setOperatingLicenseTypeIdAssoc(array $operatingLicenseTypeIdAssoc): void
    {
        $this->operatingLicenseTypeIdAssoc = $operatingLicenseTypeIdAssoc;
    }

    public function setOperatingLicenseItemStatusWorkAssoc(array $operatingLicenseItemStatusWorkAssoc): void
    {
        $this->operatingLicenseItemStatusWorkAssoc = $operatingLicenseItemStatusWorkAssoc;
    }

    public function setOperatingLicenseOrigins(array $operatingLicenseOrigins): void
    {
        $this->operatingLicenseOrigins = $operatingLicenseOrigins;
    }

    public function setOperatingLicenseOrigin(string $operatingLicenseOrigin): void
    {
        if (!in_array($operatingLicenseOrigin, $this->operatingLicenseOrigins)) {
            return;
        }
        $this->operatingLicenseOrigin = $operatingLicenseOrigin;
    }

    public function getOperatingLicenseOrigin(): string
    {
        return $this->operatingLicenseOrigin;
    }

    public function setOperatingLicenseDateFrom(string $operatingLicenseDateFrom): void
    {
        if (!$this->date->isValid($operatingLicenseDateFrom)) {
            return;
        }
        $this->operatingLicenseDateFrom = $operatingLicenseDateFrom;
    }

    public function setOperatingLicenseDateTo(string $operatingLicenseDateTo): void
    {
        if (!$this->date->isValid($operatingLicenseDateTo)) {
            return;
        }
        $this->operatingLicenseDateTo = $operatingLicenseDateTo;
    }

    public function setOperatingLicenseNumber(string $operatingLicenseNumber): void
    {
        $this->operatingLicenseNumber = $this->stringFilter->filter($operatingLicenseNumber);
    }

    public function setOperatingLicenseWorkNumber(string $operatingLicenseWorkNumber): void
    {
        $this->operatingLicenseWorkNumber = $this->stringFilter->filter($operatingLicenseWorkNumber);
    }

    public function setOperatingLicenseRemark(string $operatingLicenseRemark): void
    {
        $this->operatingLicenseRemark = $this->stringFilter->filter($operatingLicenseRemark);
    }

    public function setUserUuidCertifiedStaff(string $userUuidCertifiedStaff): void
    {
        if (!$this->uuidValid->isValid($userUuidCertifiedStaff)) {
            return;
        }
        $this->userUuidCertifiedStaff = $userUuidCertifiedStaff;
    }

    public function setOperatingLicenseTypeId(int $operatingLicenseTypeId): void
    {
        if (!in_array($operatingLicenseTypeId, $this->operatingLicenseTypeIdAssoc)) {
            return;
        }
        $this->operatingLicenseTypeId = $operatingLicenseTypeId;
    }

    public function setOperatingLicenseItemDesc(string $operatingLicenseItemDesc): void
    {
        $this->operatingLicenseItemDesc = $this->stringFilter->filter($operatingLicenseItemDesc);
    }

    public function setOperatingLicenseItemPartNumber(string $operatingLicenseItemPartNumber): void
    {
        $this->operatingLicenseItemPartNumber = $this->stringFilter->filter($operatingLicenseItemPartNumber);
    }

    public function setOperatingLicenseItemSerialNumber(string $operatingLicenseItemSerialNumber): void
    {
        $this->operatingLicenseItemSerialNumber = $this->stringFilter->filter($operatingLicenseItemSerialNumber);
    }

    public function setOperatingLicenseItemStatusWork(string $operatingLicenseItemStatusWork): void
    {
        if (!in_array($operatingLicenseItemStatusWork, array_keys($this->operatingLicenseItemStatusWorkAssoc))) {
            return;
        }
        $this->operatingLicenseItemStatusWork = $operatingLicenseItemStatusWork;
    }

    public function setProductNo(string $productNo): void
    {
        $this->productNo = $this->stringFilter->filter($productNo);
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setOperatingLicenseOrigin($qp['operating_license_origin'] ?? 0);
        $this->setOperatingLicenseDateFrom($qp['operating_license_date_from'] ?? '');
        $this->setOperatingLicenseDateTo($qp['operating_license_date_to'] ?? '');
        $this->setOperatingLicenseNumber($qp['operating_license_number'] ?? '');
        $this->setOperatingLicenseWorkNumber($qp['operating_license_work_number'] ?? '');
        $this->setOperatingLicenseRemark($qp['operating_license_remark'] ?? '');
        $this->setUserUuidCertifiedStaff($qp['user_uuid_certified_staff'] ?? '');
        $this->setOperatingLicenseTypeId($qp['operating_license_type_id'] ?? 0);
        $this->setOperatingLicenseItemDesc($qp['operating_license_item_desc'] ?? '');
        $this->setOperatingLicenseItemPartNumber($qp['operating_license_item_part_number'] ?? '');
        $this->setOperatingLicenseItemSerialNumber($qp['operating_license_item_serial_number'] ?? '');
        $this->setOperatingLicenseItemStatusWork($qp['operating_license_item_status_work'] ?? '');
        $this->setProductNo($qp['product_no'] ?? '');
    }

    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        if (empty($this->operatingLicenseOrigin)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__ . '() operatingLicenseOrigin is empty');
            return;
        }
        parent::computeSelect($select, $orderDefault);
        switch ($this->operatingLicenseOrigin) {
            case 'maintenance':
                $select->where->isNull('stockin_uuid');
                break;
            case 'production':
                $select->where->isNull('order_item_uuid');
                break;
        }
        if ($this->operatingLicenseDateFrom) {
            $select->where->greaterThanOrEqualTo('operating_license_date', $this->operatingLicenseDateFrom);
        }
        if ($this->operatingLicenseDateTo) {
            $select->where->lessThanOrEqualTo('operating_license_date', $this->operatingLicenseDateTo);
        }
        if ($this->operatingLicenseNumber) {
            $select->where(['operating_license_number' => $this->operatingLicenseNumber]);
            $select->where->like('operating_license_number_complete', '%' . $this->operatingLicenseNumber . '%');
        }
        if ($this->operatingLicenseWorkNumber) {
            $select->where->like('operating_license_work_number', '%' . $this->operatingLicenseWorkNumber . '%');
        }
        if ($this->operatingLicenseRemark) {
            $select->where->like('operating_license_remark', '%' . $this->operatingLicenseRemark . '%');
        }
        if ($this->userUuidCertifiedStaff) {
            $select->where(['user_uuid_certified_staff' => $this->userUuidCertifiedStaff]);
        }
        if ($this->operatingLicenseTypeId) {
            $select->where(['operating_license_type_id' => $this->operatingLicenseTypeId]);
        }
        if ($this->operatingLicenseItemDesc) {
            $select->where->like('operating_license_item_desc', '%' . $this->operatingLicenseItemDesc . '%');
        }
        if ($this->operatingLicenseItemPartNumber) {
            $select->where->like('operating_license_item_part_number', '%' . $this->operatingLicenseItemPartNumber . '%');
        }
        if ($this->operatingLicenseItemSerialNumber) {
            $select->where->like('operating_license_item_serial_number', '%' . $this->operatingLicenseItemSerialNumber . '%');
        }
        if ($this->operatingLicenseItemStatusWork) {
            $select->where(['operating_license_item_status_work' => $this->operatingLicenseItemStatusWork]);
        }
        if ($this->productNo) {
            $select->where->like('product_no_no', '%' . $this->productNo . '%');
        }
    }

}

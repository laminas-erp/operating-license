<?php

namespace Lerp\OperatingLicense\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Log\Logger;
use Laminas\Validator\Date;

/**
 * Params for a complete OperatingLicenseItem for the creation of a OperatingLicense
 */
class ParamsOperatingLicenseItemEntity extends ParamsBase
{
    protected Logger $logger;
    protected Date $date;
    protected FilterChainStringSanitize $stringFilter;

    protected int $operatingLicenseTypeId;
    protected string $operatingLicenseDate;
    protected string $orderUuid;
    protected string $orderItemUuid;
    protected string $stockinUuid;
    protected string $userUuidCertifiedStaff;
    protected string $operatingLicenseUuidPrev;
    protected string $itemStatusWork;

    public function __construct()
    {
        parent::__construct();
        $this->date = new Date();
        $this->stringFilter = new FilterChainStringSanitize();
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setOperatingLicenseTypeId(int $operatingLicenseTypeId): void
    {
        $this->operatingLicenseTypeId = $operatingLicenseTypeId;
    }

    public function setOperatingLicenseDate(string $operatingLicenseDate): void
    {
        if (!$this->date->isValid($operatingLicenseDate)) {
            return;
        }
        $this->operatingLicenseDate = $operatingLicenseDate;
    }

    public function setOrderUuid(string $orderUuid): void
    {
        if (!$this->uuidValid->isValid($orderUuid = $this->stringFilter->filter($orderUuid))) {
            return;
        }
        $this->orderUuid = $orderUuid;
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        if (!$this->uuidValid->isValid($orderItemUuid = $this->stringFilter->filter($orderItemUuid))) {
            return;
        }
        $this->orderItemUuid = $orderItemUuid;
    }

    public function setStockinUuid(string $stockinUuid): void
    {
        if (!$this->uuidValid->isValid($stockinUuid = $this->stringFilter->filter($stockinUuid))) {
            return;
        }
        $this->stockinUuid = $stockinUuid;
    }

    public function setUserUuidCertifiedStaff(string $userUuidCertifiedStaff): void
    {
        if (!$this->uuidValid->isValid($userUuidCertifiedStaff = $this->stringFilter->filter($userUuidCertifiedStaff))) {
            return;
        }
        $this->userUuidCertifiedStaff = $userUuidCertifiedStaff;
    }

    public function setOperatingLicenseUuidPrev(string $operatingLicenseUuidPrev): void
    {
        if (!$this->uuidValid->isValid($operatingLicenseUuidPrev = $this->stringFilter->filter($operatingLicenseUuidPrev))) {
            $this->operatingLicenseUuidPrev = '';
            return;
        }
        $this->operatingLicenseUuidPrev = $operatingLicenseUuidPrev;
    }

    public function setItemStatusWork(string $itemStatusWork): void
    {
        $this->itemStatusWork = $this->stringFilter->filter($itemStatusWork);
    }

    public function getOperatingLicenseTypeId(): int
    {
        return $this->operatingLicenseTypeId;
    }

    public function getOperatingLicenseDate(): string
    {
        return $this->operatingLicenseDate;
    }

    public function getOrderUuid(): string
    {
        return $this->orderUuid;
    }

    public function getOrderItemUuid(): string
    {
        return $this->orderItemUuid;
    }

    public function getStockinUuid(): string
    {
        return $this->stockinUuid;
    }

    public function getUserUuidCertifiedStaff(): string
    {
        return $this->userUuidCertifiedStaff;
    }

    public function getOperatingLicenseUuidPrev(): string
    {
        return $this->operatingLicenseUuidPrev;
    }

    public function getItemStatusWork(): string
    {
        return $this->itemStatusWork;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setOperatingLicenseTypeId($qp['operating_license_type_id'] ?? 0);
        $this->setOperatingLicenseDate($qp['operating_license_date'] ?? '');
        $this->setOrderUuid($qp['order_uuid'] ?? '');
        $this->setOrderItemUuid($qp['order_item_uuid'] ?? '');
        $this->setStockinUuid($qp['stockin_uuid'] ?? '');
        $this->setUserUuidCertifiedStaff($qp['user_uuid_certified_staff'] ?? '');
        $this->setOperatingLicenseUuidPrev($qp['operating_license_uuid_prev'] ?? '');
        // item
        $this->setItemStatusWork($qp['operating_license_item_status_work'] ?? '');
        if (empty($this->orderUuid) && empty($this->stockinUuid)) {
            $this->addMessage('Es muss einen Auftrag oder einen Lagereingang geben.');
            $this->success = false;
        }
        if (empty($this->operatingLicenseDate)) {
            $this->addMessage('Das datum ist nicht valide');
            $this->success = false;
        }
        if (empty($this->workNumber) && empty($this->remark) && empty($this->userUuidCertifiedStaff)) {
            $this->addMessage('Die Daten sind unvollständig');
            $this->success = false;
        }
        if (empty($this->itemDesc) && empty($this->itemPartNumber) && empty($this->itemQuantity) && empty($this->itemStatusWork)) {
            $this->addMessage('Die Daten für das Item sind unvollständig');
            $this->success = false;
        }
    }

    /**
     * Additional check after $this->setFromParamsArray(array $qp)
     *
     * @return bool
     */
    public function validForOrderItem(): bool
    {
        if (empty($this->orderItemUuid)) {
            $this->addMessage('Es gibt kein Auftrag Item.');
            return false;
        }
        return true;
    }
}

<?php

namespace Lerp\OperatingLicense\Entity;

use Lerp\OperatingLicense\Service\OperatingLicenseService;

/**
 * Complete data entity with data from tables:
 * - operating_license_item
 * - operating_license
 * - operating_license_type
 */
class ViewOperatingLicenseItemEntity extends OperatingLicenseEntity
{
    protected array $mapping = [
        'operating_license_item_uuid'          => 'operating_license_item_uuid',
        'operating_license_uuid'               => 'operating_license_uuid',
        'operating_license_item_position'      => 'operating_license_item_position',
        'operating_license_item_desc'          => 'operating_license_item_desc',
        'operating_license_item_part_number'   => 'operating_license_item_part_number',
        'operating_license_item_quantity'      => 'operating_license_item_quantity',
        'operating_license_item_serial_number' => 'operating_license_item_serial_number',
        'operating_license_item_status_work'   => 'operating_license_item_status_work',
        'order_uuid'                           => 'order_uuid',
        'order_item_uuid'                      => 'order_item_uuid',
        'stockin_uuid'                         => 'stockin_uuid',
        'operating_license_time_create'        => 'operating_license_time_create',
        'operating_license_date'               => 'operating_license_date',
        'operating_license_number'             => 'operating_license_number',
        'operating_license_number_prefix'      => 'operating_license_number_prefix',
        'operating_license_number_complete'    => 'operating_license_number_complete',
        'operating_license_work_number'        => 'operating_license_work_number',
        'operating_license_remark'             => 'operating_license_remark',
        'user_uuid_certified_staff'            => 'user_uuid_certified_staff',
        'operating_license_uuid_prev'          => 'operating_license_uuid_prev',
        'operating_license_type_id'            => 'operating_license_type_id',
        'operating_license_type_name'          => 'operating_license_type_name',
        'user_email'                           => 'user_email',
        'user_login'                           => 'user_login',
        'user_details_name_first'              => 'user_details_name_first',
        'user_details_name_last'               => 'user_details_name_last',
    ];

    protected $primaryKey = 'operating_license_item_uuid';

    public function isCompleteForInsert(): bool
    {
        return !empty($this->getOperatingLicenseTypeId())
            && ((!empty($this->getOrderUuid()) && !empty($this->getOrderItemUuid())) || !empty($this->getStockinUuid()))
            && !empty($this->getOperatingLicenseDate())
            && !empty($this->getOperatingLicenseWorkNumber())
            && !empty($this->getOperatingLicenseRemark())
            && !empty($this->getUserUuidCertifiedStaff())
            && !empty($this->getOperatingLicenseItemDesc())
            && !empty($this->getOperatingLicenseItemPartNumber())
            && !empty($this->getOperatingLicenseItemSerialNumber())
            && !empty($this->getOperatingLicenseItemStatusWork());
    }

    public function isCompleteForDocument(): bool
    {
        return !empty($this->getOperatingLicenseUuid()) && !empty($this->getOperatingLicenseItemUuid())
            && $this->isCompleteForInsert();
    }

    public function getOperatingLicenseItemUuid(): string
    {
        if (!isset($this->storage['operating_license_item_uuid'])) {
            return '';
        }
        return $this->storage['operating_license_item_uuid'];
    }

    public function setOperatingLicenseItemUuid(string $operatingLicenseItemUuid): void
    {
        $this->storage['operating_license_item_uuid'] = $operatingLicenseItemUuid;
    }

    public function getOperatingLicenseItemPosition(): int
    {
        if (!isset($this->storage['operating_license_item_position'])) {
            return 0;
        }
        return $this->storage['operating_license_item_position'];
    }

    public function setOperatingLicenseItemPosition(int $operatingLicenseItemPosition): void
    {
        $this->storage['operating_license_item_position'] = $operatingLicenseItemPosition;
    }

    public function getOperatingLicenseItemDesc(): string
    {
        if (!isset($this->storage['operating_license_item_desc'])) {
            return '';
        }
        return $this->storage['operating_license_item_desc'];
    }

    public function setOperatingLicenseItemDesc(string $operatingLicenseItemDesc): void
    {
        $this->storage['operating_license_item_desc'] = $operatingLicenseItemDesc;
    }

    public function getOperatingLicenseItemPartNumber(): string
    {
        if (!isset($this->storage['operating_license_item_part_number'])) {
            return '';
        }
        return $this->storage['operating_license_item_part_number'];
    }

    public function setOperatingLicenseItemPartNumber(string $operatingLicenseItemPartNumber): void
    {
        $this->storage['operating_license_item_part_number'] = $operatingLicenseItemPartNumber;
    }

    public function getOperatingLicenseItemQuantity(): int
    {
        if (!isset($this->storage['operating_license_item_quantity'])) {
            return 0;
        }
        return $this->storage['operating_license_item_quantity'];
    }

    public function setOperatingLicenseItemQuantity(int $operatingLicenseItemQuantity): void
    {
        $this->storage['operating_license_item_quantity'] = $operatingLicenseItemQuantity;
    }

    public function getOperatingLicenseItemSerialNumber(): string
    {
        if (!isset($this->storage['operating_license_item_serial_number'])) {
            return '';
        }
        return $this->storage['operating_license_item_serial_number'];
    }

    public function setOperatingLicenseItemSerialNumber(string $operatingLicenseItemSerialNumber): void
    {
        $this->storage['operating_license_item_serial_number'] = $operatingLicenseItemSerialNumber;
    }

    public function getOperatingLicenseItemStatusWork(): string
    {
        if (!isset($this->storage['operating_license_item_status_work'])) {
            return '';
        }
        return $this->storage['operating_license_item_status_work'];
    }

    public function setOperatingLicenseItemStatusWork(string $operatingLicenseItemStatusWork): void
    {
        $this->storage['operating_license_item_status_work'] = $operatingLicenseItemStatusWork;
    }

    public function getOperatingLicenseTypeName(): string
    {
        if (!isset($this->storage['operating_license_type_name'])) {
            return '';
        }
        return $this->storage['operating_license_type_name'];
    }

    public function setOperatingLicenseTypeName(string $operatingLicenseTypeName): void
    {
        $this->storage['operating_license_type_name'] = $operatingLicenseTypeName;
    }
}

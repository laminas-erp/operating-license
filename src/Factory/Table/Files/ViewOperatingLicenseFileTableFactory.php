<?php

namespace Lerp\OperatingLicense\Factory\Table\Files;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileTable;

class ViewOperatingLicenseFileTableFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new ViewOperatingLicenseFileTable();
        $table->setLogger($container->get('logger'));
        $table->setDbAdapter($container->get('dbDefault'));
        return $table;
    }
}

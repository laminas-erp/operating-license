<?php

namespace Lerp\OperatingLicense\Factory\Form;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;

class ViewOperatingLicenseItemFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ViewOperatingLicenseItemForm();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setOperatingLicenseOrigins($toolsTable->getEnumValuesPostgreSQL('enum_operating_license_origin'));
        /** @var OperatingLicenseTypeTable $operatingLicenseTypeTable */
        $operatingLicenseTypeTable = $container->get(OperatingLicenseTypeTable::class);
        $form->setOperatingLicenseTypeIdAssoc($operatingLicenseTypeTable->getOperatingLicenseTypeAssoc());
        $form->setStatusWorkAssoc($container->get('config')['lerp_operating_license']['status_work']);
        return $form;
    }
}

<?php

namespace Lerp\OperatingLicense\Factory\Controller\Rest;

use Bitkorn\Trinket\Service\LangService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Controller\Rest\OperatingLicenseOrderItemRestController;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseOrderItemRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OperatingLicenseOrderItemRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOperatingLicenseItemService($container->get(OperatingLicenseItemService::class));
        $controller->setOperatingLicenseService($container->get(OperatingLicenseService::class));
        $controller->setViewOperatingLicenseItemForm($container->get(ViewOperatingLicenseItemForm::class));
        $controller->setTranslator($container->get('MvcTranslator'));
        $controller->setLangService($container->get(LangService::class));
        return $controller;
    }
}

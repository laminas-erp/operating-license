<?php

namespace Lerp\OperatingLicense\Factory\Controller\Rest\Files;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Controller\Rest\Files\FileOperatingLicenseRestController;
use Lerp\OperatingLicense\Form\Files\FileOperatingLicenseForm;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;

class FileOperatingLicenseRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FileOperatingLicenseRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $olConfig = $container->get('config')['lerp_operating_license'];
        $controller->setFileCategoryIdCompute($olConfig['file_category_id_compute']);
        $controller->setFileCategoryIdSigned($olConfig['file_category_id_signed']);
        $controller->setFileOperatingLicenseForm($container->get(FileOperatingLicenseForm::class));
        $controller->setFileOperatingLicenseRelService($container->get(FileOperatingLicenseRelService::class));
        return $controller;
    }
}

<?php

namespace Lerp\OperatingLicense\Factory\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseSearchAjaxController;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;

class OperatingLicenseSearchAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OperatingLicenseSearchAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setParamsOperatingLicenseItemSearchEntity($container->get(ParamsOperatingLicenseItemSearchEntity::class));
        $controller->setOperatingLicenseItemService($container->get(OperatingLicenseItemService::class));
        return $controller;
    }
}

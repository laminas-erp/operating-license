<?php

namespace Lerp\OperatingLicense\Factory\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Controller\Ajax\OperatingLicenseCreateAjaxController;
use Lerp\OperatingLicense\Form\ViewOperatingLicenseItemForm;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class OperatingLicenseCreateAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new OperatingLicenseCreateAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setOperatingLicenseItemService($container->get(OperatingLicenseItemService::class));
        $controller->setOperatingLicenseService($container->get(OperatingLicenseService::class));
        $controller->setViewOperatingLicenseItemForm($container->get(ViewOperatingLicenseItemForm::class));
        $controller->setOperatingLicenseAccumulateService($container->get(OperatingLicenseAccumulateService::class));
        return $controller;
    }
}

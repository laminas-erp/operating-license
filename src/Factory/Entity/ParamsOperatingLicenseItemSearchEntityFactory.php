<?php

namespace Lerp\OperatingLicense\Factory\Entity;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemSearchEntity;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;

class ParamsOperatingLicenseItemSearchEntityFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entity = new ParamsOperatingLicenseItemSearchEntity();
        $entity->setLogger($container->get('logger'));
        /** @var OperatingLicenseTypeTable $operatingLicenseTypeTable */
        $operatingLicenseTypeTable = $container->get(OperatingLicenseTypeTable::class);
        $entity->setOperatingLicenseTypeIdAssoc($operatingLicenseTypeTable->getOperatingLicenseTypeAssoc());
        $entity->setOperatingLicenseItemStatusWorkAssoc($container->get('config')['lerp_operating_license']['status_work']);
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $entity->setOperatingLicenseOrigins($toolsTable->getEnumValuesPostgreSQL('enum_operating_license_origin'));
        return $entity;
    }
}

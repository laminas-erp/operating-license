<?php

namespace Lerp\OperatingLicense\Factory\Entity;

use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Entity\ParamsOperatingLicenseItemEntity;

class ParamsOperatingLicenseItemEntityFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entity = new ParamsOperatingLicenseItemEntity();
        $entity->setLogger($container->get('logger'));
        return $entity;
    }
}

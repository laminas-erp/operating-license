<?php

namespace Lerp\OperatingLicense\Factory\Service;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Table\OperatingLicenseItemTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemOrderTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemStockTable;
use Lerp\OperatingLicense\Table\ViewOperatingLicenseItemTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProviderInterface;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Stock\Service\StockService;

class OperatingLicenseItemServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OperatingLicenseItemService();
        $service->setLogger($container->get('logger'));
        $toolsTable = $container->get(ToolsTable::class);
        $service->setOperatingLicenseOrigins($toolsTable->getEnumValuesPostgreSQL('enum_operating_license_origin'));
        $service->setStatusWorkAssoc($container->get('config')['lerp_operating_license']['status_work']);
        $service->setOperatingLicenseTable($container->get(OperatingLicenseTable::class));
        $service->setOperatingLicenseItemTable($container->get(OperatingLicenseItemTable::class));
        /** @var OperatingLicenseTypeTable $operatingLicenseTypeTable */
        $operatingLicenseTypeTable = $container->get(OperatingLicenseTypeTable::class);
        $service->setOperatingLicenseTypeTable($operatingLicenseTypeTable);
        $service->setOperatingLicenseTypeIdAssoc($operatingLicenseTypeTable->getOperatingLicenseTypeAssoc());
        $service->setViewOperatingLicenseItemTable($container->get(ViewOperatingLicenseItemTable::class));
        $service->setViewOperatingLicenseItemOrderTable($container->get(ViewOperatingLicenseItemOrderTable::class));
        $service->setViewOperatingLicenseItemStockTable($container->get(ViewOperatingLicenseItemStockTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        $service->setOrderItemService($container->get(OrderItemService::class));
        $service->setStockService($container->get(StockService::class));
        return $service;
    }
}

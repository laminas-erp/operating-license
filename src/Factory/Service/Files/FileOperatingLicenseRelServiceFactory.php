<?php

namespace Lerp\OperatingLicense\Factory\Service\Files;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;
use Lerp\OperatingLicense\Table\Files\FileOperatingLicenseRelTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileOrderTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileStockTable;
use Lerp\OperatingLicense\Table\Files\ViewOperatingLicenseFileTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProviderInterface;

class FileOperatingLicenseRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileOperatingLicenseRelService();
        $service->setLogger($container->get('logger'));
        $service->setModuleBrand($container->get('config')['lerp_operating_license']['module_brand']);
        $service->setFileOperatingLicenseRelTable($container->get(FileOperatingLicenseRelTable::class));
        $service->setViewOperatingLicenseFileTable($container->get(ViewOperatingLicenseFileTable::class));
        $service->setViewOperatingLicenseFileOrderTable($container->get(ViewOperatingLicenseFileOrderTable::class));
        $service->setViewOperatingLicenseFileStockTable($container->get(ViewOperatingLicenseFileStockTable::class));
        $service->setFileService($container->get(FileService::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        return $service;
    }
}

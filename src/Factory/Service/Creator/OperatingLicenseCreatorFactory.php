<?php

namespace Lerp\OperatingLicense\Factory\Service\Creator;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Equipment\Service\User\UserEquipService;
use Lerp\OperatingLicense\Service\Creator\OperatingLicenseCreator;
use Lerp\OperatingLicense\Service\Files\FileOperatingLicenseRelService;
use Lerp\OperatingLicense\Table\OperatingLicenseTypeTable;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Stock\Service\StockService;

class OperatingLicenseCreatorFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OperatingLicenseCreator();
        $service->setLogger($container->get('logger'));
        $service->setDbAdapter($container->get('dbDefault'));
        $config = $container->get('config');
        $service->setFilePath($config['bitkorn_files']['files_upload_path']);
        $service->setFolderBrand($config['lerp_operating_license']['module_brand']);
        /** @var OperatingLicenseTypeTable $operatingLicenseTypeTable */
        $operatingLicenseTypeTable = $container->get(OperatingLicenseTypeTable::class);
        $service->setOperatingLicenseTypeAssoc($operatingLicenseTypeTable->getOperatingLicenseTypeAssoc());
        $service->setFileService($container->get(FileService::class));
        $service->setStockService($container->get(StockService::class));
        $service->setOrderService($container->get(OrderService::class));
        $service->setUserEquipService($container->get(UserEquipService::class));
        $service->setFileOperatingLicenseRelService($container->get(FileOperatingLicenseRelService::class));
        return $service;
    }

}

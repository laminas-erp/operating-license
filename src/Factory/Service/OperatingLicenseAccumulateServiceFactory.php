<?php

namespace Lerp\OperatingLicense\Factory\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Service\OperatingLicenseAccumulateService;
use Lerp\OperatingLicense\Service\OperatingLicenseItemService;
use Lerp\OperatingLicense\Service\OperatingLicenseService;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;

class OperatingLicenseAccumulateServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OperatingLicenseAccumulateService();
        $service->setLogger($container->get('logger'));
        $service->setOperatingLicenseTable($container->get(OperatingLicenseTable::class));
        $service->setOperatingLicenseService($container->get(OperatingLicenseService::class));
        $service->setOperatingLicenseItemService($container->get(OperatingLicenseItemService::class));
        return $service;
    }
}

<?php

namespace Lerp\OperatingLicense\Factory\Unique;

use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\OperatingLicense\Table\OperatingLicenseTable;
use Lerp\OperatingLicense\Unique\UniqueNumberProvider;

class UniqueNumberProviderFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $unique = new UniqueNumberProvider();
        $unique->setLogger($container->get('logger'));
        $unique->setOperatingLicenseTable($container->get(OperatingLicenseTable::class));
        return $unique;
    }
}

<?php

namespace Lerp\OperatingLicense\Form;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\Digits;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Date;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;
use Lerp\OperatingLicense\Service\OperatingLicenseService;

class ViewOperatingLicenseItemForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $operatingLicenseOrigins;
    protected array $operatingLicenseTypeIdAssoc;
    protected array $statusWorkAssoc;
    protected int $operatingLicenseSourceType;
    protected bool $isOperatingLicenseUuidPrev = false;

    public function setOperatingLicenseOrigins(array $operatingLicenseOrigins): void
    {
        $this->operatingLicenseOrigins = $operatingLicenseOrigins;
    }

    public function setOperatingLicenseTypeIdAssoc(array $operatingLicenseTypeIdAssoc): void
    {
        $this->operatingLicenseTypeIdAssoc = $operatingLicenseTypeIdAssoc;
    }

    public function setStatusWorkAssoc(array $statusWorkAssoc): void
    {
        $this->statusWorkAssoc = $statusWorkAssoc;
    }

    /**
     * @param int $operatingLicenseSourceType One of the constants OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM or OperatingLicenseService::SOURCE_TYPE_STOCKIN
     * @return void
     */
    public function setOperatingLicenseSourceType(int $operatingLicenseSourceType): void
    {
        $this->operatingLicenseSourceType = $operatingLicenseSourceType;
    }

    public function setIsOperatingLicenseUuidPrev(bool $isOperatingLicenseUuidPrev): void
    {
        $this->isOperatingLicenseUuidPrev = $isOperatingLicenseUuidPrev;
    }

    public function init()
    {
        if (!isset($this->operatingLicenseSourceType)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() $this->operatingLicenseSourceType must set before you call init().');
        }
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'operating_license_uuid']);
            $this->add(['name' => 'operating_license_item_uuid']);
        }
        $this->add(['name' => 'operating_license_type_id']);
        if ($this->secondaryKeysAvailable) {
            switch ($this->operatingLicenseSourceType) {
                case OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM:
                    $this->add(['name' => 'order_uuid']);
                    $this->add(['name' => 'order_item_uuid']);
                    break;
                case OperatingLicenseService::SOURCE_TYPE_STOCKIN:
                    $this->add(['name' => 'stockin_uuid']);
                    break;
            }
        }
        $this->add(['name' => 'operating_license_date']);
        $this->add(['name' => 'operating_license_work_number']);
        $this->add(['name' => 'operating_license_remark']);
        $this->add(['name' => 'user_uuid_certified_staff']);
        if ($this->isOperatingLicenseUuidPrev) {
            $this->add(['name' => 'operating_license_uuid_prev']);
        }
        // item
        $this->add(['name' => 'operating_license_item_desc']);
        $this->add(['name' => 'operating_license_item_part_number']);
        $this->add(['name' => 'operating_license_item_quantity']);
        $this->add(['name' => 'operating_license_item_serial_number']);
        $this->add(['name' => 'operating_license_item_status_work']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        if ($this->primaryKeyAvailable) {
            $filter['operating_license_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class,]
                ]
            ];
            $filter['operating_license_item_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class,]
                ]
            ];
        }

        $filter['operating_license_type_id'] = [
            'required'      => true,
            'filters'       => [
                ['name' => Digits::class],
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->operatingLicenseTypeIdAssoc),
                        'messages' => [
                            InArray::NOT_IN_ARRAY => 'Diesen Lizenztyp gibt es nicht'
                        ]
                    ]
                ]
            ]
        ];

        if ($this->secondaryKeysAvailable) {
            switch ($this->operatingLicenseSourceType) {
                case OperatingLicenseService::SOURCE_TYPE_ORDER_ITEM:
                    $filter['order_uuid'] = [
                        'required'      => true,
                        'filters'       => [
                            ['name' => SanitizeStringFilter::class],
                        ], 'validators' => [
                            ['name' => Uuid::class,]
                        ]
                    ];

                    $filter['order_item_uuid'] = [
                        'required'      => true,
                        'filters'       => [
                            ['name' => SanitizeStringFilter::class],
                        ], 'validators' => [
                            ['name' => Uuid::class,]
                        ]
                    ];
                    break;
                case OperatingLicenseService::SOURCE_TYPE_STOCKIN:
                    $filter['stockin_uuid'] = [
                        'required'      => true,
                        'filters'       => [
                            ['name' => SanitizeStringFilter::class],
                        ], 'validators' => [
                            ['name' => Uuid::class,]
                        ]
                    ];
                    break;
            }
        }

        $filter['operating_license_date'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Date::class]
            ]
        ];

        $filter['operating_license_work_number'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 16,
                    ]
                ]
            ]
        ];

        $filter['operating_license_remark'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 4000,
                    ]
                ]
            ]
        ];

        $filter['user_uuid_certified_staff'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class,]
            ]
        ];

        if ($this->isOperatingLicenseUuidPrev) {
            $filter['operating_license_uuid_prev'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class,]
                ]
            ];
        }
        // item
        $filter['operating_license_item_desc'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        $filter['operating_license_item_part_number'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['operating_license_item_quantity'] = [
            'required'   => true,
            'filters'    => [['name' => Digits::class]],
            'validators' => [
                [
                    'name'    => NotEmpty::class,
                    'options' => [
                        'type' => NotEmpty::ALL
                    ]
                ]
            ]
        ];

        $filter['operating_license_item_serial_number'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 80,
                    ]
                ]
            ]
        ];

        $filter['operating_license_item_status_work'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 80,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
